const path = require('path');
// var fs = require('fs');
const useDefaultConfig = require('../node_modules/@ionic/app-scripts/config/webpack.config.js');

module.exports = function () {
    // var currentEnvironment = process.env.IONIC_ENV;
    let resolvePath = './src/environments/environment';
    // const optionalConfigPath = resolvePath + '.config.ts';

    resolvePath += '.ts';

    /*if (fs.existsSync(optionalConfigPath)) {
        resolvePath = optionalConfigPath;
    }*/

    useDefaultConfig.resolve.alias = {
        "@app/env": path.resolve(resolvePath)
    };

    return useDefaultConfig;
};
