import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, IonicPageModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from '@ionic/storage';
import { ChartsModule } from 'ng2-charts';
import { CallNumber } from '@ionic-native/call-number';

import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppVersion } from '@ionic-native/app-version';

import {MyApp} from './app.component';
import {TruncateModule} from 'ng2-truncate';
import {ToastrModule} from 'ngx-toastr';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {
    HttpService,
    DashboardEventService,
    ProductService,
    AuthService,
    CategoryService,
    BranchService,
    RoleService,
    DeliveryService,
    ErrorResponseService,
    SearchService,
    UserService,
    AlertService,
    TransactionService,
    PricingService,
    StorageService
} from '../shared';
import {TrackingEventService} from '../shared/services/helpers/tracking-event.service';
import {PlatformService} from '../shared/services/helpers/platform.service';

import {
    AlertErrorHandlerComponent,
    ErrorHandlerComponent
} from '../components/directives';

import {SigninPage} from '../pages/signin/signin';
import {OwnerPage} from '../pages/owner/owner';
import {FranchiseePage} from '../pages/franchisee-coordinator/franchisee/franchisee';
import {CoordinatorPage} from '../pages/franchisee-coordinator/coordinator/coordinator';
import {TransactionsPage} from '../pages/franchisee-coordinator/transactions/transactions';
import {InventoryPage} from '../pages/franchisee-coordinator/inventory/inventory';
import {CoordinatorInventoryPage} from '../pages/franchisee-coordinator/coordinator-inventory/coordinator-inventory';
import {SingleTransactionPage} from '../pages/franchisee-coordinator/single-transaction/single-transaction';
import {AddBranchPage} from '../pages/franchisee-coordinator/add-branch/add-branch';
import {BranchesPage} from '../pages/franchisee-coordinator/branches/branches';
import {DailyTransactionDetailsPage} from '../pages/franchisee-coordinator/daily-transaction-details/daily-transaction-details';
import {DatePipe} from '@angular/common';

@NgModule({
    declarations: [
        MyApp,
        BranchesPage,
        SigninPage,
        OwnerPage,
        FranchiseePage,
        CoordinatorPage,
        TransactionsPage,
        DailyTransactionDetailsPage,
        InventoryPage,
        CoordinatorInventoryPage,
        SingleTransactionPage,
        AlertErrorHandlerComponent,
        ErrorHandlerComponent,
        AddBranchPage
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        IonicPageModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot(),
        TruncateModule,
        HttpModule,
        FormsModule,
        ChartsModule,
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true
        })

    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        BranchesPage,
        SigninPage,
        OwnerPage,
        FranchiseePage,
        CoordinatorPage,
        TransactionsPage,
        InventoryPage,
        CoordinatorInventoryPage,
        SingleTransactionPage,
        AddBranchPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        CallNumber,
        ScreenOrientation,
        AppVersion,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        StorageService,
        HttpService,
        DashboardEventService,
        TrackingEventService,
        ProductService,
        AuthService,
        CategoryService,
        BranchService,
        RoleService,
        DeliveryService,
        ErrorResponseService,
        SearchService,
        UserService,
        AlertService,
        TransactionService,
        PricingService,
        PlatformService,
        DatePipe
    ]
})
export class AppModule {
}
