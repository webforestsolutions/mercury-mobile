import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { AlertController, Nav, Platform, MenuController } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Subscription } from 'rxjs/Subscription';

// CLASSES
import { Constants } from '../shared/constants';
import { Branch, Page } from '../classes';

// PAGES
import { AddBranchPage, BranchesPage, FranchiseePage, TransactionsPage } from '../pages/franchisee-coordinator';
import { OwnerPage } from '../pages/owner/owner';
import { SigninPage } from '../pages/signin/signin';

// SERVICES
import { AuthService, BranchService, StorageService, TrackingEventService } from '../shared/services';

@Component({
    templateUrl: 'app.html'
})
export class MyApp implements OnInit, OnDestroy {
    @ViewChild(Nav) nav: Nav;

    activePage: Page;
    appName: string;
    branches: any = [];
    currentBranchKey: any = '';
    pages: Page[] = [];
    role: any = '';
    rootPage: any;
    sessionExpiredModalShown: boolean = false;
    tokenRefreshInitialized: boolean = false;

    private authConfirmSubscription: Subscription;
    private branchKeyChangeSubscription: Subscription;
    private branchListSubscription: Subscription;
    private expiredSessionSubscription: Subscription;
    private refreshTokenSubscription: Subscription;
    private updateCurrentBranchKey: Subscription;
    private updateRoleSubscription: Subscription;

    constructor(
        public menuCtrl: MenuController,
        public platform: Platform,
        public splashScreen: SplashScreen,
        private alertCtrl: AlertController,
        private authService: AuthService,
        private branchService: BranchService,
        private screenOrientation: ScreenOrientation,
        private storageService: StorageService,
        private trackingEventService: TrackingEventService
    ) { }

    ngOnInit(): void {
        this.initializeApp();
        this.initializeSubscriptions();
    }

    ngOnDestroy(): void {
        this.terminateSubscriptions();
    }

    goToHomePage(): void {
        const role = this.role;

        switch (role) {
            case 'coordinator':
                this.nav.push(BranchesPage)
                    .then(() => this.menuCtrl.close())
                    .catch(error => console.log('Error pushing page', error));

                break;

            case 'franchisee':
                this.nav.push(FranchiseePage)
                    .then(() => this.menuCtrl.close())
                    .catch(error => console.log('Error pushing page', error));

                break;

            default:
                this.nav.push(OwnerPage)
                    .then(() => this.menuCtrl.close())
                    .catch(error => console.log('Error pushing page', error));

                break;
        }
    }

    goToAddBranchPage(): void {
        this.nav.push(AddBranchPage)
            .then(() => this.menuCtrl.close())
            .catch(error => console.log('Error pushing page', error));
    }

    logout(): void {
        const code = 'logout';
        this.showConfirmLogoutPrompt(code);
    }
    
    openTransaction(page: Page): void {
        this.nav.setRoot(page.component, { key: page.key, name: page.title, role: this.role, id: page.id });
        this.activePage = page;
    }

    private initializeApp(): void {

        this.platform.ready()
            .then(
                () => {
                    this.splashScreen.hide();
                    this.checkSession();
                    this.lockToPortraitMode();
                }
            )
            .catch(error => console.log('Error initializing the platform', error));
    }
    
    private lockToPortraitMode(): void {
        if (this.platform.is('mobile') && !this.platform.is('mobileweb')) {
            console.log('App is running on mobile');
            console.log('Locking orientation to portrait mode');
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        }
    }

    private initializeSubscriptions(): void {
        this.refreshTokenSubscription = this.authService.refreshTokenEvent
            .subscribe(
                (refresh: boolean) => {
                    if (refresh) {

                        if (this.tokenRefreshInitialized) {
                            return;
                        }

                        this.tokenRefreshInitialized = true;
                        this.authService.refreshTokenFromExpiredRequest();
                    }
                }
            );

        this.authConfirmSubscription = this.authService.confirm
            .subscribe(
                (confirm: boolean) => {
                    if (confirm) {
                        this.tokenRefreshInitialized = false;
                    }
                }
            );

        this.updateRoleSubscription = this.trackingEventService.onUpdateRole
            .subscribe(
                (role: string) => {
                    this.role = role;
                    console.log('role is ', this.role);

                    if (this.role === 'coordinator') {

                        this.branchKeyChangeSubscription = this.trackingEventService.onBranchKeyChange
                            .subscribe(
                                (onChange: boolean) => {
                                    if (onChange) {
                                        this.getBranches();
                                    }
                                }
                            );
            
                        this.branchListSubscription = this.trackingEventService.onBranchListChange
                            .subscribe(
                                (onChange: boolean) => {
                                    if (onChange) {
                                        this.setPages();
                                    }
                                }
                            );
            
                        return;
                    }
                }
            );

        this.updateCurrentBranchKey = this.trackingEventService.onCurrentBranchKeyChange
            .subscribe(
                (branchKey: string) => {
                    this.currentBranchKey = branchKey;
                }
            );

    }

    private async checkSession(): Promise<any> {
        
        if (this.sessionExpiredModalShown) {
            return;
        }

        const loader = this.trackingEventService.loading('Checking Session, Please Wait...');

        try {
            const token = await this.authService.getToken();

            if (!token) {
                loader.dismiss();
                this.nav.setRoot(SigninPage);
                return;
            }

            this.authService.setToken(token);
            const decoded: { role: string } = this.authService.decodeToken(token);
            this.trackingEventService.onUpdateRole.emit(decoded.role);
            loader.dismiss();
            this.setRootPage(token);

        } catch (e) {
            loader.dismiss();
            console.log('Error checking session ', e);
        }

    }

    private async getBranches(): Promise<any> {
        this.branches = [];

        try {
            const getKeys = await this.storageService.get(Constants.branchKeys);
            const keys = JSON.parse(getKeys);
    
            if (!keys) {
                return;
            }

            const branches: { data: any[] } = await this.branchService.getBranchesByKeys(keys.join(',')).toPromise();
            this.setBranches(branches.data);

        } catch (e) {
            console.log('Error retrieving branches ', e);
        }
    }

    private terminateSubscriptions(): void {
        this.branchListSubscription.unsubscribe();
        this.updateCurrentBranchKey.unsubscribe();
        this.updateRoleSubscription.unsubscribe();
        this.authConfirmSubscription.unsubscribe();
        this.refreshTokenSubscription.unsubscribe();
        this.expiredSessionSubscription.unsubscribe();
        this.branchKeyChangeSubscription.unsubscribe();
    }

    private navigateToRootByRole(role: string): void {
        console.log('navigate to root page by role', role);

        switch (role) {
            case 'admin':
            case 'owner':
                this.rootPage = OwnerPage;
                break;
            case 'franchisee':
                this.rootPage = FranchiseePage;
                break;
            case 'coordinator':
                this.rootPage = BranchesPage;
                break;
            default:
                console.log('You are not permitted to use this application');
                this.authService.logout();
                this.nav.setRoot(SigninPage);
        }

    }

    private setRootPage(token: string): void {
        const isLoggedIn = this.authService.isLoggedIn();

        if (!isLoggedIn) {
            this.splashScreen.hide();
            this.rootPage = SigninPage;
            return;
        }

        const isTokenExpired = this.authService.isTokenExpired();

        if (isLoggedIn && !isTokenExpired) {
            console.log('logged in and token is NOT expired');
            const role = this.authService.getRole();
            this.splashScreen.hide();
            this.navigateToRootByRole(role);
            return;
        }

        if (isLoggedIn && isTokenExpired) {
            console.log('logged in and token is expired')
            this.authService.refreshToken(token)
                .subscribe(
                    (tokenData) => {
                        this.splashScreen.hide();
                        this.navigateToRootByRole(tokenData.role);
                    },
                    (error) => {
                        console.log('Error refreshing token ', error);
                        this.splashScreen.hide();
                        this.rootPage = SigninPage;
                    }
                );
        }
    }

    private setPages(): void {
        this.pages = [];
        let branches = this.branches;

        branches.forEach(
            (branch) => {
                let branchPageObj: Page = new Page();
                branchPageObj.setId(branch.id);
                branchPageObj.setTitle(branch.name);
                branchPageObj.setKey(branch.key);
                branchPageObj.setComponent(TransactionsPage);
                branchPageObj.setRole(this.role);
                this.pages.push(branchPageObj);
            }
        );
    }

    private setBranches(branches: any[]): void {
        this.branches = [];

        branches.forEach(
            (data) => {
                const branch = new Branch();
                branch.key = data.key;
                branch.name = data.name;
                branch.id = data.id;
                branch.type = data.type;
                branch.status = data.status;
                this.branches.push(branch);
            }
        );
    }

    private showConfirmLogoutPrompt(code: string): void {
        let confirm = this.alertCtrl.create({
            title: 'Logout',
            message: 'Are you sure you want to log out? Branches will be removed.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('User has opted not to log out');
                        this.menuCtrl.close();
                    }
                },
                {
                    text: 'Okay',
                    handler: () => {
                        this.authService.logout();
                        this.nav.setRoot(SigninPage);
                    }
                }
            ]
        });

        if (code != 'logout') {
            return console.log('This is not a logout');
        }

        confirm.present();
    }

}
