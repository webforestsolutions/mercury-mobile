import {Component, Input, OnInit} from '@angular/core';

//import {ErrorResponseService} from "../../../shared";
import {ErrorResponse} from "../../../classes";

@Component({
  selector: 'alert-error-handler',
  templateUrl: './alert-error-handler.component.html'
})
export class AlertErrorHandlerComponent implements OnInit {

  @Input() errorResponse: ErrorResponse = new ErrorResponse();
  @Input() type: string = 'alert';

  constructor() { }

  ngOnInit() {
  }

  onDismiss() {
    this.errorResponse = new ErrorResponse();
  }

}
