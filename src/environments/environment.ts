const URL = {
    live: 'http://172.104.60.122/',
    staging: 'http://172.104.181.122/',
    local: 'http://localhost:8000/',
    localbuild: 'http://192.168.1.8/mercury-api-repo/public/' // must have the same IP address with your local machine
};

export const ENV = {
    production: true,
    url: URL.live
}
