import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController, Platform } from 'ionic-angular';

import { AuthService, StorageService, TrackingEventService } from '../../shared/services';
import { FranchiseePage } from '../franchisee-coordinator/franchisee/franchisee';
import { BranchesPage } from '../franchisee-coordinator/branches/branches';
import { OwnerPage } from '../owner/owner';

@Component({
    selector: 'page-signin',
    templateUrl: 'signin.html'
})
export class SigninPage implements OnInit, OnDestroy {

    isCordovaEnabled: boolean = false;
    username: string = '';
    password: string = '';
    version: any;

    constructor(
        public navCtrl: NavController,
        private alert: AlertController,
        private authService: AuthService,
        private menu: MenuController,
        private platform: Platform,
        private storage: StorageService,
        private trackingEventService: TrackingEventService
    ) { }

    ngOnInit(): void {
        this.menu.enable(false);
        this.trackingEventService.dismissLoading();
        this.getAppVersion();
    }

    ngOnDestroy(): void { }

    onLogin(): void {
        const loader = this.trackingEventService.loading();

        this.authService.signIn(this.username, this.password)
            .subscribe(
                response => {

                    if (!response.role) {
                        this.trackingEventService.dismissLoading();
                        this.storage.remove('token');
                        this.storage.remove('email');
                        const code = 'access_denied';
                        this.showAlert(code);
                        return;
                    }

                    loader.dismiss();
                    this.trackingEventService.onUpdateRole.emit(response.role);
                    this.navigateToRootByRole(response.role);

                },
                error => {
                    console.log('Error signing in', error);
                    loader.dismiss();
                    let errorStatus = error.status;

                    if (errorStatus == 0) {
                        const code = 'no_internet';
                        return this.showAlert(code);
                    }

                }
            );
    }

    private navigateToRootByRole(role: string): void {

        switch (role) {
            case 'admin':
            case 'owner':
                this.navCtrl.setRoot(OwnerPage, {role: role});
                break;
            case 'franchisee':
                this.navCtrl.setRoot(FranchiseePage, {role: role});
                break;
            default:
                this.navCtrl.setRoot(BranchesPage, {role: role});
                break;
        }

    }

    private showAlert(code: string): void {
        let buttons: any;
        let title: any;
        let message: any;

        switch(code) {
            case 'no_internet':
                title = 'No Internet Connection';
                message = 'Please make sure you are connected to a network';
                buttons = ['Okay'];
                break;

            case 'access_denied':
                title = 'Access Denied';
                message = 'You have no permission to access this account';
                buttons = ['Okay'];
                break;
        }

        let alert = this.alert.create({ title: title, message: message, buttons: buttons });
        alert.present();

    }

    private async getAppVersion(): Promise<any> {
        if (this.platform.is('mobile') || !this.platform.is('mobileweb')) {
            this.isCordovaEnabled = true;

            this.version = this.trackingEventService.getAppVersion()
                .then(
                    response => {
                        this.version = response;
                        console.log('App Version ', response);
                    },
                    error => console.log('Error retrieving app version ', error)
                );
        }
    }

}
