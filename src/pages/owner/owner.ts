import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { AlertController, MenuController, NavController, Loading } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as moment from 'moment';

import { SigninPage } from '../signin/signin';
import { Report, SalesSummary } from '../../shared/interfaces';
import { Branch, BranchSaleSummary, Delivery, ProductVariation, SalesChange, SalesOptions, } from '../../classes';
import { AuthService, BranchService, DeliveryService, ProductService, TrackingEventService, TransactionService, UserService } from '../../shared/services';

@Component({
    selector: 'page-owner',
    templateUrl: 'owner.html'
})
export class OwnerPage implements OnInit, OnDestroy {

    @ViewChild('fromDatePicker') fromDatePicker;
    @ViewChild('toDatePicker') toDatePicker;

    branches: Branch[] = [];
    branchSales: SalesSummary[] = [];
    change: SalesChange = new SalesChange();
    currentBranchId: number = null;
    currentBranchIndex: number = null;
    currentBranchName: string = 'Warehouse';
    currentDate: string = moment(moment(), moment.ISO_8601).format();
    currentDateFilter: string = null;
    currentPanel: string = 'home';
    currentRange: string = 'day';
    currentReportsTab: string = 'general';
    deliveries: Delivery[] = [];
    franchiseeSales: SalesSummary[] = [];
    fromDate: string = new Date().toISOString();
    height = window.innerHeight;
    invPanel: boolean = false;
    loading: boolean = false;
    monthlyPercentageSummary: any[] = [];
    monthlySummary: any[] = [];
    products: ProductVariation[] = [];
    reports: boolean = false;
    report: { total_sales: string, total_discounts: string, total_sold_items: number };
    salesSummary: BranchSaleSummary[] = [];
    stockInventory: any[] = [];
    storeSalesBreakdown: boolean = false;
    summaries: any[] = [];
    toDate: string = new Date().toISOString();
    topDealers: any[] = [];
    totalBranchSales: number;
    totalBeginningInventory: number = 0;
    totalEndingInventory: number = 0;
    totalDeliverySales: number;
    totalMonthlySales: number;
    totalSales: number;
    totalSoldItems: number = null;

    private allSummariesSubscription: Subscription;
    private loadingSalesChanges: boolean = false;
    private loadingSalesSummary: boolean = false;
    private loadingTopItems: boolean = false;
    private refreshPageSubscription: Subscription;

    private breakdownHeight = 0;
    private topProductName: string = '';

    constructor(
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        private authService: AuthService,
        private branchService: BranchService,
        private datePipe: DatePipe,
        private deliveryService: DeliveryService,
        private menu: MenuController,
        private productService: ProductService,
        private trackingEventService: TrackingEventService,
        private transactionService: TransactionService,
        private userService: UserService
    ) {}

    ngOnInit(): void {
        this.report = { total_sales: '0.00', total_discounts: '0.00', total_sold_items: 0 };
        this.fromDate = this.datePipe.transform(this.fromDate, 'yyyy-MM-dd');
        this.toDate = this.datePipe.transform(this.toDate, 'yyyy-MM-dd');
        this.menu.enable(false);
        this.setCurrentDateFilter(this.currentDate);
        this.getAllBranches();
        this.storesPanel();
        this.intializeSubscriptions();
    }

    ngOnDestroy(): void {
        this.terminateSubscriptions();
        this.trackingEventService.dismissLoading();
    }

    touched(): void {
        this.restartRefreshPageSubscription();
    }

    /**
     * Triggers when you change the range on the top-most part of the page
     * @param range
     */
    onChangeRange(range: string = 'day', date: string): void {
        this.currentRange = range;
        this.setCurrentDateFilter(date);

        switch (this.currentPanel) {
            case 'reports':
                this.reportsPanel();
                break;
            case 'inventory':
                this.inventoryPanel();
                break;
            default:
                this.storesPanel();
        }
    }

    /**
     * Triggers when you switch between 'General Summary' or 'Daily Sales' tab on the Reports page
     * @param tab = 'general' | 'daily'
     */
    onChangeReportsTab(tab: string): void {
        this.currentReportsTab = tab;

        switch (tab) {
            case 'daily':

                if (typeof this.currentBranchIndex !== 'number') {
                    this.currentBranchName = this.branches[0].name;
                    this.currentBranchIndex = this.branches.findIndex(branch => branch.id === this.branches[0].id);
                }

                this.onGenerateReport();
                break;
            default:
                break;
        }
    }

    onGenerateReport(): void {

        if (typeof this.currentBranchIndex !== 'number') {

            const alert = this.alertCtrl.create({
                title: 'Branch required!',
                message: 'Please make sure to select a branch first',
                buttons: [ { text: 'Okay' } ]
            });

            alert.present();
            return;
        }

        const branchId: number = this.branches[this.currentBranchIndex].id;
        const loader = this.trackingEventService.loading('Generating report...');

        const generate: Subscription = this.transactionService.getBranchSalesSummaryPerDateByPeriod(branchId, this.fromDate, this.toDate)
            .subscribe(
                (response: { data: Report[] }) => {
                    loader.dismiss();
                    generate.unsubscribe();
                    this.summaries = response.data;
                    this.report.total_sales = parseFloat(this.summaries.reduce((a, b) => a + parseFloat(b.total_sales), 0)).toFixed(2);
                    this.report.total_discounts = parseFloat(this.summaries.reduce((a, b) => a + parseFloat(b.total_discounts), 0)).toFixed(2);
                    this.report.total_sold_items = this.summaries.reduce((a, b) => a + b.total_sold_items, 0);
                },
                error => {
                    loader.dismiss();
                    console.log('Error generating report ', error);
                }
            );
    }

     /**
     * Displays the Stores page when tapping 'Stores' on the bottom nav
     */
    storesPanel(type?: string): void {

        // workaround to reset date filter when tapping Stores from the bottom nav
        if (type === 'reset') {
            this.resetCurrentDate();
            this.currentDateFilter = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
            this.currentRange = 'day';
        }

        this.currentPanel = 'home';
        this.currentReportsTab = 'general';
        this.reports = false;
        this.breakdownHeight = 0;
        this.invPanel = false;
        this.getAllSummaries();
    }

    /**
     * Displays the Inventory page when tapping 'Inventory' on the bottom nav
     */
    inventoryPanel(): void {
        this.invPanel = true;
        this.reports = false;
        this.currentPanel = 'inventory';
        this.currentReportsTab = 'general';
        this.onChangeInventoryBranch(0);
    }

    /**
     * Displays the Delivery page when tapping 'Deliveries' on the bottom nav
     */
    deliveryPanel(): void {
        this.invPanel = false;
        this.reports = false;
        this.currentPanel = 'delivery';
        this.getDeliveries();
    }

    /**
     * Displays the Reports page when tapping 'Report' on the bottom nav
     */
    reportsPanel(): void {
        const height = this.salesSummary.length;
        this.currentPanel = 'reports';
        this.reports = true;
        this.breakdownHeight = 50 * height;
        this.invPanel = false;
        this.currentReportsTab = 'general';
        this.getReportsData();
        this.getTopItems(this.currentRange, this.currentDateFilter);
        this.getTopDealers();
    }

    onChangeInventoryBranch(index: number): void {
        this.currentBranchIndex = index;
        this.currentBranchName = this.branches[index].name;
        this.currentBranchId = this.branches[index].id;
        this.getProductStocks();
    }

    salesBreakdownPanel(): void {

        if (this.storeSalesBreakdown) {
          this.storeSalesBreakdown = false;
        } else {
            this.storeSalesBreakdown = true;
        }

    }

    logout(): void {
        this.currentPanel = 'logout';
        const previousPanel = this.currentPanel;

        const confirm = this.alertCtrl.create({
            title: 'Logout',
            message: 'Are you sure you want to log out?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('User opted not to logout');
                        this.currentPanel = previousPanel;
                    }
                },
                {
                    text: 'Okay',
                    handler: () => {
                        this.authService.logout();
                        this.navCtrl.setRoot(SigninPage);
                    }
                }
            ]
        });

        confirm.present()
            .catch(error => console.log('Error presenting alert', error));
    }

    onConfirmDelivery(id: number, status: string): void {

        if (status !== 'pending') {
            return;
        }

        const confirm = this.alertCtrl.create({
            title: 'Delivery Confirmation',
            message: 'Are you sure you want to confirm this delivery?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => console.log('Cancelled confirmation of delivery')
                },
                {
                    text: 'Okay',
                    handler: () => {
                        const loader: Loading = this.trackingEventService.loading('Confirming delivery...');

                        const confirmDelivery: Subscription = this.deliveryService.confirmDelivery(id)
                            .subscribe(
                                () => {
                                    confirmDelivery.unsubscribe();
                                    loader.dismiss();
                                    this.getDeliveries();
                                    confirm.dismiss();
                                },
                                (error) => {
                                    loader.dismiss();
                                    confirm.dismiss();
                                    console.log('Error confirming delivery', error);

                                    if (typeof error.json().message !== 'undefined') {
                                        const alert = this.alertCtrl.create({ 
                                            title: 'Error',
                                            message: error.json().message,
                                            buttons: [ { text: 'Okay' } ]
                                        });

                                        alert.present()
                                            .catch(error => console.log('Error presenting alert ', error));
                                    }
                                }
                            );
                    }
                }
            ]
        });

        confirm.present()
            .catch(error => console.log('Error presenting alert', error));
    }

    onPayDelivery(id: number, paymentStatus: string): void {

        if (paymentStatus !== 'pending') {
            return;
        }

        const confirm = this.alertCtrl.create({
            title: 'Delivery Payment',
            message: 'Are you sure you want to set this delivery as paid?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => console.log('Cancelled payment of delivery')
                },
                {
                    text: 'Okay',
                    handler: () => {
                        const loader: Loading = this.trackingEventService.loading('Updating payment status...');

                        const confirmDelivery: Subscription = this.deliveryService.payDelivery(id)
                            .subscribe(
                                () => {
                                    confirmDelivery.unsubscribe();
                                    loader.dismiss();
                                    this.getDeliveries();
                                    confirm.dismiss();
                                },
                                (error) => {
                                    loader.dismiss();
                                    confirm.dismiss();
                                    console.log('Error updating delivery status', error);

                                    if (typeof error.json().message !== 'undefined') {
                                        const alert = this.alertCtrl.create({ 
                                            title: 'Error',
                                            message: error.json().message,
                                            buttons: [ { text: 'Okay' } ]
                                        });

                                        alert.present()
                                            .catch(error => console.log('Error presenting alert ', error));
                                    }
                                }
                            );
                    }
                }
            ]
        });

        confirm.present()
            .catch(error => console.log('Error presenting alert', error));
    }

    private intializeSubscriptions(): void {
        this.subscribeToRefreshPage();
    }

    private terminateSubscriptions(): void {
        this.refreshPageSubscription.unsubscribe();
        this.allSummariesSubscription.unsubscribe();
    }

    private subscribeToRefreshPage(): void {
        this.refreshPageSubscription = Observable.interval(60000)
            .subscribe(
                () => {

                    if (this.loading) {
                        console.log('Data still loading, do not refresh yet...');
                        return;
                    }

                    console.log('1 minute has passed, refreshing page...');
                    const panel = this.currentPanel;
                    this.resetCurrentDate();
                    this.currentDateFilter = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');

                    switch (panel) {
                        case 'reports':

                            if (this.currentReportsTab = 'daily') {
                                console.log('Not refreshing since currently on daily reports tab');
                                return;
                            }

                            this.reportsPanel();
                            break;
                        case 'inventory':
                            this.inventoryPanel();
                            break;
                        case 'delivery':
                            this.deliveryPanel();
                            break;
                        default:
                            this.storesPanel();
                    }
                },
                (error) => console.log('Error refreshing the page', error)
            );
    }

    private restartRefreshPageSubscription(): void {

        if (this.refreshPageSubscription) {
            this.refreshPageSubscription.unsubscribe();
        }

        this.subscribeToRefreshPage();
    }

    private getAllSummaries(): void {
        this.loading = true;
        const loader = this.trackingEventService.loading('Updating sales...');
        const branchSummary: Observable<any> = this.transactionService.getBranchSalesSummary('company', this.currentDateFilter, this.currentRange);
        const franchiseeSummary: Observable<any> = this.transactionService.getBranchSalesSummary('franchisee', this.currentDateFilter, this.currentRange);
        const totalSummary: Observable<any> = this.transactionService.getTotalSalesSummary(this.currentDateFilter, this.currentRange);

        this.allSummariesSubscription = forkJoin(branchSummary, franchiseeSummary, totalSummary)
            .subscribe(
                ([branchSummaryResponse, franchiseeSummaryResponse, totalSummaryResponse]) => {
                    this.branchSales = branchSummaryResponse.data;
                    this.franchiseeSales = franchiseeSummaryResponse.data;
                    this.totalBranchSales = this.calculateTotalSales(this.branchSales);
                    this.totalDeliverySales = this.calculateTotalSales(this.franchiseeSales);
                    this.totalSales = this.totalBranchSales + this.totalDeliverySales;
                    this.change = totalSummaryResponse.data;
                    loader.dismiss();
                    this.loading = false;
                },
                error => {
                    console.log('Error updating summaries', error)
                    loader.dismiss();
                    this.loading = false;
                }
            );
    }

    private getProductStocks(branchId: number = null): void {
        this.loading = true;
        const loader = this.trackingEventService.loading('Loading inventory...');

        const getItems: Subscription = this.productService.getProductStocks(null, branchId)
            .switchMap(
                (response: { data: ProductVariation[] }) => {
                    const variationIds = response.data.map(item => item.id).join(',');
                    return this.productService.getDailyInventory(this.currentDateFilter, this.currentBranchId, variationIds);
                }
            )
            .subscribe(
                (response) => {
                    this.totalSoldItems = response.data.total_sold_items;
                    this.stockInventory = response.data.inventory;
                    this.calculateTotalInventoryItems();
                    loader.dismiss();
                    getItems.unsubscribe();
                    this.loading = false;
                },
                error => {
                    console.log('Error retrieving inventory', error);
                    this.loading = false;
                }
            );

    }

    private calculateTotalInventoryItems(): void {
        this.totalBeginningInventory = this.stockInventory.reduce((a, b) => a + Math.abs(b.beginning), 0);
        this.totalEndingInventory = this.stockInventory.reduce((a, b) => a + Math.abs(b.ending), 0);
    }

    private getReportsData(): void {
        this.loading = true;
        const loader = this.trackingEventService.loading('Getting sales report...');
        this.getTotalSalesForReportsPanel();

        const monthlySummary: Subscription = this.transactionService.getTotalMonthlyBranchSalesForOneYear(this.currentDate, 'month')
            .subscribe(
                response =>  {
                    this.setTotalBranchMonthlySalesSummary(response.data);
                    this.calculateGraphValue();
                    monthlySummary.unsubscribe();
                    loader.dismiss();
                    this.loading = false;
                },
                error => {
                    console.log('Error retrieving monthly summary', error);
                    this.checkOwnerLoadingState();
                    loader.dismiss();
                    this.loading = false;
                }
            );
    }

    private getTotalSalesForReportsPanel(load?: boolean): void {
        this.loading = true;
        let loader;

        if (typeof load !== 'undefined' && load) {
            loader = this.trackingEventService.loading('Getting sales...');
        }

        const getBranchSales: Observable<any> = this.transactionService.getBranchSalesSummary('company', this.currentDateFilter, this.currentRange);
        const getDeliverySales: Observable<any> = this.transactionService.getBranchSalesSummary('franchisee', this.currentDateFilter, this.currentRange);

        const totalSales: Subscription = forkJoin(getBranchSales, getDeliverySales)
            .subscribe(
                ([branchSales, franchiseeSales]) => {
                    this.totalBranchSales = this.calculateTotalSales(branchSales.data);
                    this.totalDeliverySales = this.calculateTotalSales(franchiseeSales.data);
                    totalSales.unsubscribe();
                    this.loading = false;

                    if (load && typeof loader !== 'undefined') {
                        loader.dismiss();
                    }

                },
                error => {
                    this.loading = false;
                    console.log('Error retrieving total sales', error);

                    if (load && typeof loader !== 'undefined') {
                        loader.dismiss();
                    }
                }
            );
    }

    private getTopItems(range: string = 'day', rangeDate: string = null): void {
        this.loading = true;
        const options: SalesOptions = { range: range, rangeDate: rangeDate };

        const getTopSellingItems: Subscription = this.transactionService.getTopSaleItems(options)
            .subscribe(
                response => {
                    this.setProducts(response.data);
                    this.checkOwnerLoadingState();
                    this.loading = false;
                    getTopSellingItems.unsubscribe();
                },
                error => {
                    console.log('Error retrieving top items', error);
                    this.loading = false;
                    this.checkOwnerLoadingState();
                }
            );
    }

    private getTopDealers(range: string = 'month'): void {
        this.loading = true;

        const getTopDealers: Subscription = this.userService.getTopDealers(range, this.currentDateFilter)
            .subscribe(
                response => {
                    this.topDealers = response.data.dealers;
                    this.loading = false;
                    getTopDealers.unsubscribe();
                },
                error => {
                    console.log('Error retrieving top dealers', error);
                    this.loading = false;
                }
            );

    }

    private getAllBranches(): void {
        this.loading = true;

        const getBranches: Subscription = this.branchService.getBranches({ source: 'dropdown' })
            .subscribe(
                response => {
                    this.branches = response.data;
                    this.loading = false;
                    getBranches.unsubscribe();
                },
                error => {
                    console.log('Error retrieving branches', error);
                    this.loading = false;
                }
            );
    }

    private getDeliveries(): void {
        const loader: Loading = this.trackingEventService.loading('Retrieving deliveries...');

        const get: Subscription = this.deliveryService.getDeliveries()
            .subscribe(
                (response: { data: Delivery[] }) => {
                    loader.dismiss();
                    this.deliveries = response.data;
                    get.unsubscribe();
                },
                error => {
                    loader.dismiss();
                    console.log('Error retrieving deliveries', error)
                }
            );
    }

    private calculateGraphValue(): void {
        this.monthlyPercentageSummary = [];
        const max = Math.max.apply(Math, this.monthlySummary.map((summary) => summary.net_sales));

        this.monthlySummary.forEach(
            (item) => {
                item.percentage = (item.net_sales * 100) / max;
                this.monthlyPercentageSummary.push(item);
            }
        );

        this.checkOwnerLoadingState();
    }

    private calculateTotalSales(summary: SalesSummary[]): number {

        if (summary.length <= 0) {
            return 0.00;
        }

        return summary.map((branch) => parseFloat(branch.total_sales)).reduce((a, b) => a + b);

    }

    private checkOwnerLoadingState(): boolean {

        if (this.loadingSalesSummary) {
            return false;
        }

        if (this.loadingSalesChanges) {
            return false;
        }

        if (this.loadingTopItems) {
            return false;
        }

    }

    private setTotalBranchMonthlySalesSummary(summary: any): void {
        let netSales: any[] = [];
        this.monthlySummary = summary;
        summary.forEach((month) => netSales.push(month.net_sales));
        this.totalMonthlySales = netSales.reduce((a, b) => a + b);
    }

    private setCurrentDateFilter(date: string): void {
        this.currentDateFilter = this.datePipe.transform(date, 'yyyy-MM-dd');
    }

    private setProducts(products: any[]): void {
        this.products = [];

        products.forEach(
            data => {
                const variation = new ProductVariation();
                variation.name = data.product_name;
                variation.size = data.size;
                variation.metrics = data.metrics;
                variation.quantity = data.sum;

                if (data.product_name !== 'Paper bag' && data.product_name != 'Membership Card' && this.products.length < 5) {
                    this.products.push(variation);
                }
            }
        );

        if (this.products.length > 0) {
            this.topProductName = this.products[0].name;
        }

    }

    private resetCurrentDate(): void {
        this.currentDate = moment(moment(), moment.ISO_8601).format();
    }

}
