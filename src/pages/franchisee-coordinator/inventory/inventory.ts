import {NavController, NavParams} from 'ionic-angular';
import {Component, OnDestroy, OnInit} from '@angular/core';

// CLASSES
import {ProductVariation} from '../../../classes/product-variation.class';

// PAGES
import {FranchiseePage} from '../franchisee/franchisee';

// SERVICES
import {ProductService} from '../../../shared/services/api/product.service';
import {TrackingEventService} from '../../../shared/services/helpers/tracking-event.service';

@Component({
    selector: 'page-inventory',
    templateUrl: 'inventory.html'
})

export class InventoryPage implements OnInit, OnDestroy {

    branchName: string = '';
    currentRole: string = '';
    height = window.innerHeight;
    warehouseProducts: any[] = [];
    currentBranchId: number = null;
    products: ProductVariation[] = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private trackingEventService: TrackingEventService,
        private productService: ProductService
    ) {

        this.currentBranchId = this.navParams.get('branchId');
        this.branchName = this.navParams.get('name');
        this.currentRole = this.navParams.get('role');

        console.log('current branch id: ', this.currentBranchId);

        const branchId = this.currentBranchId;

        this.getProductStocks(branchId);

    }

    ngOnInit() {

    }


    ionViewDidLoad() {

    }

    ngOnDestroy() {

    }


    // Navigation

    onToggleMenu() {
        console.log('toggle menu');
        this.trackingEventService.onBranchListChange.emit(true);
    }

    navigateBack() {
        this.navCtrl.setRoot(FranchiseePage);
    }


    // Stocks

    getProductStocks(branchId: number = null) {

        console.log('GET STOCKS BY BRANCH ID - ', branchId);

        this.warehouseProducts = [];

        const loader = this.trackingEventService.loading();

        this.productService.getProductStocks(null, branchId)
            .subscribe(
                (response) => {
                    this.setWarehouseProducts(response.data);
                    loader.dismiss();
                },
                (error) => {
                    console.log('fetch stocks error: ', error);
                    loader.dismiss();
                }
            )

    }

    setWarehouseProducts(jsonData: any[]) {

        this.warehouseProducts = [];

        jsonData.forEach(
            (jsonProduct) => {

                const product = new ProductVariation();

                product.name = jsonProduct.name;
                product.size = jsonProduct.size;
                product.metrics = jsonProduct.metrics;

                if (this.currentBranchId) {
                    product.quantity = jsonProduct.branch_quantity;
                }

                if (!this.currentBranchId) {
                    product.quantity = jsonProduct.company_quantity;
                }


                this.warehouseProducts.push(product);
            }
        );

        console.log('warehouse products: ', this.warehouseProducts);

    }

}
