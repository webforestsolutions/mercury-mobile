import { Constants } from '../../../shared/constants';
import { Component, OnInit } from '@angular/core';
import { ActionSheetController, AlertController, MenuController, NavController, NavParams } from 'ionic-angular';

// PAGES
import { AddBranchPage } from '../add-branch/add-branch';
import { SingleTransactionPage } from '../single-transaction/single-transaction';

// CLASSES
import { Branch, Transaction, SalesOptions } from '../../../classes';

// SERVICES
import { TrackingEventService, TransactionService } from '../../../shared/services';

@Component({
    selector: 'page-coordinator',
    templateUrl: 'coordinator.html'
})

export class CoordinatorPage implements OnInit {
    code: string;
    key: string = '';
    role: string = '';
    branch: Branch[] = [];
    branchKey: string = '';
    branchName: string = '';
    height = window.innerHeight;
    transactions: Transaction[] = [];

    constructor(
        public actionctrl: ActionSheetController,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private menuCtrl: MenuController,
        private transactionService: TransactionService,
        private trackingEventService: TrackingEventService
    ) {}

    ngOnInit(): void {
        this.menuCtrl.enable(true);
        this.trackingEventService.onBranchKeyChange.emit(true);
        this.branchKey = this.navParams.get('key');
        this.branchName = this.navParams.get('name');
    }

    ionViewDidEnter(): void {
        this.getSaleTransactions(this.branchKey);
    }

    // Template Functions

    onToggleMenu(): void {
        this.trackingEventService.onBranchListChange.emit(true);
    }

    onClickTransaction(transaction: Transaction): void {

        let actionsheet = this.actionctrl.create({
            buttons:[
                {
                    text: 'Preview Transaction',
                    role: 'destruction',
                    icon: 'eye',
                    handler: () => {
                        this.viewSingleTransaction(transaction);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'md-close',
                    handler: () => {
                        console.log('Cancelled');
                    }
                }
            ]
        });

        if (transaction.status == 'active') {
            actionsheet = this.actionctrl.create({
                buttons: [
                    {
                        text: 'Void Transaction',
                        role: 'destruction',
                        icon: 'ios-trash',
                        handler: () => {
                            this.showConfirm(transaction);

                        }
                    },
                    {
                        text: 'Preview Transaction',
                        role: 'destruction',
                        icon: 'eye',
                        handler: () => {
                            this.viewSingleTransaction(transaction);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'md-close',
                        handler: () => {
                            console.log('Cancelled');
                        }
                    }

                ]
            });
        }

        actionsheet.present();
    }

    private showConfirm(transaction): void {

        let orNum = transaction.or_no;

        let title = 'VOID OR #' + orNum;

        let message = Constants.confirmVoidTransaction;

        let confirm = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancelled void');
                    }
                },
                {
                    text: 'Confirm',
                    handler: () => {
                        this.voidTransaction(transaction.id);
                    }
                }
            ]
        });

        confirm.present();
    }

    private showAlert(code, message?: string, title?: string): void {

        let buttons: any;

        switch(code) {
            case 'error':
                title = 'Error';
                message = Constants.voidErrorMessage;
                buttons = ['Okay']
                break;

            case 'no_branch':
                title = 'No Branches Added'
                message = Constants.noBranchesAdded;
                buttons = [
                    {
                        text: 'Proceed',
                        handler: () => {
                            this.navCtrl.push(AddBranchPage);
                        }
                    }
                ]
                break;

            default:
                title = 'Success';
                message = Constants.voidSuccessMessage;
                buttons = [
                    {
                        text: 'Okay',
                        handler: () => {
                            this.getSaleTransactions(this.branchKey);
                        }
                    }
                ];

        }

        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: buttons
        });

        alert.present();
    }

    private getSaleTransactions(branchKey): void {

        const loader = this.trackingEventService.loading();

        const options: SalesOptions = {
            branchKey: branchKey
        };

        this.transactionService.getTransactionHistory(options)
            .subscribe(
                (response) => {
                    this.transactions = response.data;
                    loader.dismiss();
                },
                (error: Response) => {
                    this.showAlert(Constants.errorCode);
                    loader.dismiss();
                }

            );

    }

    private viewSingleTransaction(transaction: Transaction): void {
        transaction.branch_key = this.branchKey;
        this.navCtrl.push(SingleTransactionPage, transaction);
    }

    private voidTransaction(transactionId: number): void {
        const loader = this.trackingEventService.loading();

        this.transactionService.voidTransaction(transactionId)
            .subscribe(
                () => {
                    this.showAlert(Constants.successCode);
                    loader.dismiss();
                },
                error => {
                    console.log('Error voiding transaction ', error);
                    this.showAlert(Constants.errorCode);
                    loader.dismiss();
                }
            );
    }

}
