import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { Constants } from '../../../shared/constants';
// CLASSES
import { Branch, SalesOptions, Transaction } from '../../../classes';

// PAGES
import { AddBranchPage } from '../add-branch/add-branch';
import { SingleTransactionPage } from '../single-transaction/single-transaction';

// SERVICES
import { TransactionService, TrackingEventService } from '../../../shared/services';

@Component({
    selector: 'page-transactions',
    templateUrl: 'transactions.html'
})

export class TransactionsPage implements OnInit {
    activePanel = 'transactions';
    branch: Branch = new Branch();
    branchKey: number|string;
    branchKeys: any = [];
    branchName: string = '';
    branchId: number = null;
    currentRole: string = '';
    code: string;
    height = window.innerHeight;
    key: string = '';
    noTransactions: boolean = false;
    role: string = '';
    transactions: Transaction[] = [];

    constructor(
        public actionctrl: ActionSheetController,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public navParams: NavParams,
        private trackingEventService: TrackingEventService,
        private transactionService: TransactionService
    ) {}

    ngOnInit(): void {
        this.branchId = this.navParams.get('id');
        this.branchKey = this.navParams.get('key');
        this.branchName = this.navParams.get('name');
        this.currentRole = this.navParams.get('role');
    }

    ionViewDidEnter(): void {
        this.getSaleTransactions();
    }

    // GETTERS

    private getSaleTransactions(): void {
        const loader = this.trackingEventService.loading('Loading transactions...');
        let subType = null;

        if (this.currentRole == Constants.franchiseeFlag) {
            subType = Constants.franchiseeFlag;
        }

        const options: SalesOptions = {
            branchKey: this.branchKey,
            subType: subType,
            range: 'daily',
            limit: 'none'
        };

        this.transactionService.getTransactionHistory(options)
            .toPromise()
            .then(
                (response: any) => {

                    if (response.data.length <= 0) {
                        this.noTransactions = true;
                    }

                    loader.dismiss();
                    this.transactions = response.data;
                }
            )
            .catch(
                (error) => {
                    loader.dismiss();
                    console.log('Error fetching sale transactions', error);
                    this.showAlert(Constants.errorCode);
                }
            );
    }

    // CRUD

    private voidTransaction(transactionId): void {
        const loader = this.trackingEventService.loading();

        this.transactionService.voidTransaction(transactionId)
            .toPromise()
            .then(
                (response) => {
                    this.showAlert(Constants.successCode);
                    loader.dismiss();
                }
            )
            .catch(
                (error) => {
                    this.showAlert(Constants.errorCode);
                    loader.dismiss();
                }
            );
    }

    // Template Functions

    onClickTransaction(transaction: Transaction): void {
        let actionsheet = this.actionctrl.create({
            buttons:[
                {
                    text: 'Preview Transaction',
                    role: 'destruction',
                    icon: 'eye',
                    handler: () => {
                        this.viewSingleTransaction(transaction);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'md-close',
                    handler: () => {
                        console.log('Cancelled');
                    }
                }
            ]
        });

        if (transaction.status == 'active') {
            actionsheet = this.actionctrl.create({
                buttons: [
                    {
                        text: 'Void Transaction',
                        role: 'destruction',
                        icon: 'ios-trash',
                        handler: () => {
                            this.showConfirm(transaction);

                        }
                    },
                    {
                        text: 'Preview Transaction',
                        role: 'destruction',
                        icon: 'eye',
                        handler: () => {
                            this.viewSingleTransaction(transaction);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'md-close',
                        handler: () => {
                            console.log('Cancelled');
                        }
                    }

                ]
            });
        }

        actionsheet.present();
    }

    onToggleMenu(): void {
        this.trackingEventService.onBranchListChange.emit(true);
    }

    private viewSingleTransaction(transaction: Transaction): void {
        transaction.branch_key = this.branchKey;
        this.navCtrl.push(SingleTransactionPage, {transactionDetails: transaction});
    }

    // HELPERS AND ALERTS

    private showConfirm(transaction): void {
        let orNum = transaction.or_no;
        let title = 'VOID OR #' + orNum;
        let message = Constants.confirmVoidTransaction;

        let confirm = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancelled void');
                    }
                },
                {
                    text: 'Confirm',
                    handler: () => {
                        this.voidTransaction(transaction.id);
                    }
                }
            ]
        });

        confirm.present();
    }

    private showAlert(code, message?: string, title?: string): void {
        let buttons: any;

        switch (code) {
            case 'error':
                title = 'Error';
                message = 'Something went wrong while fetching your data';
                buttons = ['Okay']
                break;

            case 'no_branch':
                title = 'No Branches Added'
                message = Constants.noBranchesAdded;
                buttons = [
                    {
                        text: 'Proceed',
                        handler: () => {
                            this.navCtrl.push(AddBranchPage);
                        }
                    }
                ]
                break;

            default:
                title = 'Success';
                message = Constants.voidSuccessMessage;
                buttons = [
                    {
                        text: 'Okay',
                        handler: () => {
                            this.getSaleTransactions();
                        }
                    }
                ];

        }

        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: buttons
        });

        alert.present();
    }

}
