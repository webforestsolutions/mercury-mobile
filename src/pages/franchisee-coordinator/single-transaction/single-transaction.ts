import {Constants} from '../../../shared/constants';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuController, NavController, NavParams} from 'ionic-angular';
import {AlertController} from 'ionic-angular/components/alert/alert-controller';
import {ActionSheetController} from 'ionic-angular/components/action-sheet/action-sheet-controller';

// SERVICES
import {TransactionService} from '../../../shared/services/api/transaction.service';
import {TrackingEventService} from '../../../shared/services/helpers/tracking-event.service';

@Component({
    selector: 'page-single-transaction',
    templateUrl: 'single-transaction.html'
})

export class SingleTransactionPage implements OnInit, OnDestroy {

    transaction: any;
    branchKey: any;

    constructor(
        public navParams: NavParams,
        private navCtrl: NavController,
        private actionCtrl: ActionSheetController,
        private alertCtrl: AlertController,
        private menuCtrl: MenuController,
        private transactionService: TransactionService,
        private trackingEventService: TrackingEventService
    ) {
        if(this.navParams.data.branch_key) {
            this.branchKey = this.navParams.data.branch_key;
            this.setTransaction(this.navParams.data)
        }

        if(this.navParams.data.transactionDetails.branch_key) {
            this.branchKey = this.navParams.data.branch_key;
            this.setTransaction(this.navParams.data.transactionDetails);
        }

    }

    openMenu() {
        this.menuCtrl.open();
    }

    ionViewWillEnter(){


    }

    ionViewDidLoad(){

    }

    ngOnInit() {

    }

    ngOnDestroy() {

    }

    ionViewWillUnload() {

    }

    setTransaction(navParamsData: any) {
        this.transaction = navParamsData;
    }

    showActionSheet(transaction){
        let actionsheet = this.actionCtrl.create({
            buttons: [
                {
                    text: 'Void Transaction',
                    role: 'destruction',
                    icon: 'ios-trash',
                    handler: () => {
                        this.showConfirm(transaction);

                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'md-close',
                    handler: () => {
                        console.log('Cancel success')
                    }
                }

            ]
        });

        actionsheet.present();

    }

    showAlert(code) {

        let title = 'Success';

        let message = Constants.voidSuccessMessage;

        switch(code) {
            case 'error':
                title = 'Error';
                message = Constants.voidErrorMessage;
                break;
            case 'success':
                break;
        }

        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        console.log('this.transaction.branch_key', this.transaction.branch_key);
                        this.returnToTransactionsPage()
                    }
                }
            ]
        });

        alert.present();
    }

    showConfirm(transaction) {

        let orNum = transaction.or_no;

        let title = 'VOID OR #' + orNum;

        let message = Constants.confirmVoidTransaction;

        let confirm = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancelled void coordinator');
                    }
                },
                {
                    text: 'Confirm',
                    handler: () => {
                        this.voidTransaction(this.transaction.id);
                    }
                }
            ]
        });
        confirm.present();
    }

    returnToTransactionsPage() {
        this.navCtrl.pop();
        // this.navCtrl.push(TransactionsPage, {key: this.branchKey});
    }

    voidTransaction(transactionId) {

        const loader = this.trackingEventService.loading();

        this.transactionService.voidTransaction(transactionId)
            .subscribe(
                (response) => {
                    loader.dismiss();
                    this.showAlert(Constants.successCode);
                },
                (error) => {
                    loader.dismiss();
                    this.showAlert(Constants.errorCode);
                }
            )
    }
}
