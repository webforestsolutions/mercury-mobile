import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Constants } from '../../../shared/constants';

// CLASSES
import { Branch, User } from '../../../classes';

// PAGES
import { AddBranchPage } from '../add-branch/add-branch';
import { TransactionsPage } from '../transactions/transactions';

// SERVICES
import { StorageService, TrackingEventService, TransactionService, UserService } from '../../../shared/services';

@Component({
    selector: 'page-branches',
    templateUrl: 'branches.html'
})

export class BranchesPage implements OnInit {
    branches: Branch[] = [];
    currentUser: User = new User();
    height = window.innerHeight;
    keysExist: any = false;
    role: string = '';

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private storageService: StorageService,
        private trackingEventService: TrackingEventService,
        private transactionService: TransactionService,
        private userService: UserService
    ) {}

    ngOnInit(): void {
        this.role = this.navParams.get('role');
        this.trackingEventService.onBranchKeyChange.emit(true);
        this.getBranches();
        this.getCurrentUser();
        this.checkForBranchKeys();
    }

    // Template Functions

    goToAddBranchPage(): void {
        this.navCtrl.push(AddBranchPage);
    }

    onToggleMenu(): void {
        this.trackingEventService.onBranchListChange.emit(true);
    }

    viewBranchTransactions(index: number): void {
        this.navCtrl.push(TransactionsPage, {name: this.branches[index].name, key: this.branches[index].key, id: this.branches[index].id});
    }

    private async getBranches(): Promise<any> {
        this.branches = [];
        const loader = this.trackingEventService.loading('Loading branches...');

        try {
            const getBranchKeys = await this.storageService.get(Constants.branchKeys);

            if (!getBranchKeys) {
                loader.dismiss();
                return;
            }

            const keys = JSON.parse(getBranchKeys);
            const branchKeyString = keys.join(',');
            const summary = await this.transactionService.getBranchesSalesSummary('day', 'all', branchKeyString).toPromise();
            this.setBranches(summary.data);
            loader.dismiss();

        } catch (e) {
            console.log('Error retrieving branches ', e);
        }

    }

    private getCurrentUser(): void {
        this.userService.getCurrentUser()
            .toPromise()
            .then((response: any) => this.currentUser = response.data)
            .catch(error => console.log('Error retrieving current user ', error));
    }

    private setBranches(data: any[]): void {
        this.branches = [];

        data.forEach(
            (jsonBranch) => {
                const branch = new Branch();
                branch.key = jsonBranch.key;
                branch.name = jsonBranch.name;
                branch.address = jsonBranch.address;
                branch.id = jsonBranch.id;
                branch.type = jsonBranch.type;
                branch.status = jsonBranch.status;
                branch.net_sales = jsonBranch.net_sales;
                branch.sold_items = jsonBranch.sold_items;
                this.branches.push(branch);
            }
        );
    }

    private async checkForBranchKeys(): Promise<any> {
        try {
            const keys = await this.storageService.get(Constants.branchKeys);

            if (!keys || keys.length <= 0) {
                this.keysExist = false;
                return;
            }
    
            this.keysExist = true;

        } catch (e) {
            this.keysExist = false;
            console.log('Error checking for branch keys ', e);
        }
    }

}
