import { Component, OnInit } from '@angular/core';
import { Constants } from '../../../shared/constants';
import { MenuController, NavController, NavParams, AlertController } from 'ionic-angular';

// PAGES
import {SigninPage} from '../../signin/signin';

// SERVICES
import {AuthService} from '../../../shared/services/api/auth.service';
import {StorageService} from '../../../shared/services/helpers/storage.service';
import {TrackingEventService} from '../../../shared/services/helpers/tracking-event.service';
import {BranchService} from '../../../shared/services/api/branch.service';

@Component({
    selector: 'page-add-branch',
    templateUrl: 'add-branch.html',
})
export class AddBranchPage implements OnInit {

    branchKey: string = '';
    private branchKeys: Array<string> = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,
        private authService: AuthService,
        private storageService: StorageService,
        private trackingEventService: TrackingEventService,
        private menuCtrl: MenuController,
        private branchService: BranchService
    ) { }
    
    ngOnInit(): void {
        // transfer methods from ion view did load here
    }
    
    ionViewDidLoad(): void {
        this.menuCtrl.enable(true);
        this.getBranchKeys();
    }

    getBranchKeys(): void {
        this.storageService.get(Constants.branchKeys)
            .then(
                response => {

                    if (!response) {
                        console.log('You have not yet stored any branch keys');
                        return;
                    }

                    this.branchKeys = JSON.parse(response);
                }
            )
            .catch(error => console.log('Error retrieving branch keys ', error));
    }

    showPrompt(code: string, branchKey?: any): void {
        let alert = this.alertCtrl.create({
            title: 'Confirm',
            message: 'Are you sure you want to add this branch?',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Okay',
                    handler: () => {
                        this.validateBranchKey(branchKey);
                    }
                }
            ]
        });

        if (code === 'empty_field') {
            alert = this.alertCtrl.create({
                title: 'Whoops',
                message: 'You need to input a branch key',
                buttons: ['Okay']
            });
        }

        if (code === 'added_key') {
            alert = this.alertCtrl.create({
                title: 'Success',
                message: 'Branch key has been added',
                buttons: ['Okay']
            });
        }

        alert.present();
    }

    validateBranchKey(branchKey): void {
        let code: any;

        if (!branchKey || branchKey.length === 0) {
            code = 'empty_field';
            return this.showPrompt(code);
        }

        // check if branch key being added already exists
        let branchKeyExists = this.checkIfBranchKeyExists(branchKey);

        if (branchKeyExists) {
            code = 'key_exists';
            return this.showAlert(code);
        }

        // check if branch exists on the database
        this.checkIfBranchExists(branchKey);

    }

    logout(): void {
        this.authService.logout();
        this.navCtrl.setRoot(SigninPage);
    }

    onToggleMenu(): void {
        this.trackingEventService.onBranchListChange.emit(true);
    }

    // checks if trying to add a duplicate inside the this.branchKeys array
    checkIfBranchKeyExists(branchKey): boolean {
        const branchKeys = this.branchKeys;
        let branchKeyPos: number = branchKeys.indexOf(branchKey);

        // if the branch key being added is not yet in the array
        if (branchKeyPos == -1) {
           return false;
        }

        return true;
    }

    // checks the database if the branch key is assigned to a branch
    checkIfBranchExists(branchKey): void {
        const loader = this.trackingEventService.loading('Adding branch...');

        this.branchService.getBranchByKey(branchKey)
            .toPromise()
            .then(
                () => {
                    this.branchKeys.push(branchKey);
                    return this.storageService.set(Constants.branchKeys, JSON.stringify(this.branchKeys));
                }
            )
            .then(
                () => {
                    loader.dismiss();
                    const code = 'added_key';
                    this.showPrompt(code);
                    this.branchKey = '';
                    this.trackingEventService.onBranchKeyChange.emit(true);
                }
            )
            .catch(
                error => {
                    console.log('Error checking if branch exists ', error);
                    loader.dismiss();
                    const code = 'no_branch';
                    this.showAlert(code);
                }
            );
    }

    addBranch(branchKey): void {
        const loader = this.trackingEventService.loading('Adding branch...');
        this.branchKeys.push(branchKey);

        this.storageService.set(Constants.branchKeys, JSON.stringify(this.branchKeys))
            .then(
                () => {
                    loader.dismiss();
                    const code = 'added_key';
                    this.showPrompt(code);
                    this.branchKey = '';
                    this.trackingEventService.onBranchKeyChange.emit(true);
                }
            )
            .catch(
                error => {
                    console.log('Error while adding a branch key', error);
                    loader.dismiss();
                }
            );
    }

    showAlert(code: string, message?: string): void {
        let title: string;
        let buttons: any = ['Okay'];

        if (message) {
            message = message;
        }

        switch (code) {
            case 'key_exists':
                title = 'Whoops!';
                message = 'The branch key you are trying to add already exists';
                break;

            case 'no_branch':
                title = 'Branch not found';
                message = 'Please make sure you are adding the correct branch key';
                break;
        }

        const alert = this.alertCtrl.create({ title: title, message: message, buttons: buttons });
        alert.present();
    }
}
