import {NavController, NavParams} from 'ionic-angular';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

// CLASSES
import {ProductVariation} from '../../../classes/product-variation.class';

// PAGES
import {FranchiseePage} from '../franchisee/franchisee';

// SERVICES
import {ProductService} from '../../../shared/services/api/product.service';
import {TrackingEventService} from '../../../shared/services/helpers/tracking-event.service';

@Component({
    selector: 'page-coordinator-inventory',
    templateUrl: 'coordinator-inventory.html'
})

export class CoordinatorInventoryPage implements OnChanges {

    @Input() height: number = null;
    @Input() hidden: boolean = false;
    @Input() currentBranchId: number = null;

    loading: boolean = true;
    branchStocks: ProductVariation[] = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        private trackingEventService: TrackingEventService,
        private productService: ProductService
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        const id: number = this.currentBranchId;
        this.getBranchStocks(id);
    }

    // Navigation

    onToggleMenu(): void {
        this.trackingEventService.onBranchListChange.emit(true);
    }

    navigateBack(): void {
        this.navCtrl.setRoot(FranchiseePage);
    }

    private getBranchStocks(branchId: number): void {
        const loader = this.trackingEventService.loading('Loading inventory...');
        this.branchStocks = [];

        this.productService.getProductStocks(null, branchId)
            .toPromise()
            .then(
                (response: any) => {
                    this.loading = false;
                    this.setBranchStocks(response.data);
                    loader.dismiss();
                }
            )
            .catch(
                error => {
                    loader.dismiss();
                    this.loading = false;
                    console.log('Error retrieving stocks from branch', error);
                }
            );
    }

    private setBranchStocks(stocks: any[]): void {
        this.branchStocks = [];

        stocks.forEach(
            data => {
                const product = new ProductVariation();
                product.name = data.name;
                product.size = data.size;
                product.metrics = data.metrics;

                if (this.currentBranchId) {
                    product.quantity = data.branch_quantity;
                }

                if (!this.currentBranchId) {
                    product.quantity = data.company_quantity;
                }

                this.branchStocks.push(product);
            }
        );

    }

}
