import { Component, OnDestroy, OnInit } from '@angular/core';
import { AlertController, MenuController, NavController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { CallNumber } from '@ionic-native/call-number';
import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { Constants } from '../../../shared/constants';
import { Branch, ProductVariation, SalesOptions, Staff, Transaction } from '../../../classes';
import { AuthService, ProductService, StorageService, TrackingEventService, TransactionService, UserService } from '../../../shared/services';
import { SigninPage } from '../../signin/signin';
import { SingleTransactionPage } from '../single-transaction/single-transaction';

@Component({
    selector: 'page-franchisee',
    templateUrl: 'franchisee.html'
})
export class FranchiseePage implements OnInit, OnDestroy {

    branches: Branch[] = [];
    branchName: any = '';
    currentBranchIndex: number = 0;
    currentBranchName: string;
    currentDate: any = new Date().toISOString();
    currentPanel: string = 'home';
    currentRange: string = 'day';
    currentSalesState: string = 'increase';
    currentStaffId: string = null;
    dailySalesSummary: Branch = new Branch();
    dateSet: any;
    franchiseeName = '';
    height: number = window.innerHeight - 400;
    homePanel: boolean = true;
    invPanel: boolean = false;
    loadingStaff: boolean = false;
    loadingStaffTransactions: boolean = false;
    noInventory: boolean = false;
    noTransactions: boolean = false;
    panel: string = 'home';
    staffDailySalesSummary: Staff[] = [];
    staffList: boolean = false;
    reports: boolean = false;
    staffTransactions: Transaction[] = [];
    stockInventory: any[] = [];
    hourlySales: any = [];

    totalFranchiseeSales: any = {};
    totalNetSalesPercent: number = 0;
    totalSoldItems: number | string = null;
    transactions: Transaction[] = [];
    transactionsList: boolean = true;
    transPanel: boolean = false;

    private branchKeys: string = '';
    private currentBranchId: number = null;
    private currentBranchKey: string | number = null;
    private currentDateFilter: string = null;
    private keysExist: any = false;
    private productsContainer: any[] = [];
    private refreshPageSubscription: Subscription;
    private role: string = '';
    private updateRoleSubscription: Subscription;
    private warehouseProducts: any[] = [];

    constructor(
        public actionctrl: ActionSheetController,
        public navCtrl: NavController,
        private alertCtrl: AlertController,
        private authService: AuthService,
        private callNumber: CallNumber,
        private datePipe: DatePipe,
        private menu: MenuController,
        private productService: ProductService,
        private storageService: StorageService,
        private trackingEventService: TrackingEventService,
        private transactionService: TransactionService,
        private userService: UserService
    ) {}

    ngOnInit(): void {
        this.menu.enable(true);
        this.initializePage();
        this.initializeSubscriptions();
    }

    ngOnDestroy(): void {
        this.terminateSubscriptions();
        this.trackingEventService.dismissLoading();
    }

    ionViewDidEnter(): void {
        if (this.panel === 'transaction') {
            this.getSummaryByDatePerBranch(this.currentBranchKey);
        }
    }

    /**
     * Callback function for the date selector
     * Triggers when selecting a date from the navigation
     *
     * @param string range = 'day' | 'month' | 'year'
     * @memberof FranchiseePage
     * @returns void
     */
    getSummaryByDate(range: string = 'day'): void {
        this.currentRange = range;
        this.setCurrentDateFilter();

        const dateFilter = this.currentDateFilter;

        const options: SalesOptions = {
            range: range,
            rangeDate: dateFilter,
            subType: Constants.franchiseeFlag,
            keys: this.branchKeys
        };

        switch (this.panel) {
            case 'inventory':
                this.loadInventory();
                break;
            case 'transaction':
                //this.transactionsList = true;
               // this.staffList = false;
               this.loadingStaff = false;
                this.getSummaryByDatePerBranch(this.currentBranchKey);
                break;
            default:
                this.getBranchSalesSummary(options);
        }
    }

    /**
     * Displays the overall sales summary of all the branches under a franchisee
     * Triggers when selecting 'Sales' from the navigation menu
     *
     * @memberof FranchiseePage
     * @returns void
     */
    selectBranchSalesPanel(): void {
        this.panel = 'home';
        this.currentBranchIndex = 0;
        this.currentDate = new Date().toISOString();
        this.getSummaryByDate();
    }

    /**
     * Displays the branch sales summary, transactions, and staff transactions of a branch
     * Triggers when selecting a branch from the branch sales panel
     *
     * @param string branchKey
     * @param string branchName
     * @param number branchId
     * @memberof FranchiseePage
     * @returns void
     */
    selectBranch(branchKey: string | number, branchName: string, branchId: number): void {
        this.panel = 'transaction';
        this.branchName = branchName;
        this.currentBranchKey = branchKey;
        this.getSummaryByDatePerBranch(branchKey);
    }

    /**
     * Switch between the tabs on the branch summary view
     * Triggers when selecting the 'Transactions' tab or the 'Staff' tab
     *
     * @param string tab = 'staff' | 'transactions' | 'reports'
     * @memberof FranchiseePage
     * @returns void
     */

    selectBranchTab(tab: string): void {
        switch (tab) {
            case 'staff':
                this.staffList = true;
                this.transactionsList = false;
                this.reports = false;
                break;
            case 'transactions':
                this.currentStaffId = null;
                this.staffList = false;
                this.transactionsList = true;
                this.reports = false;
                break;
            case 'reports':
                this.reports = true;
                this.staffList = false;
                this.transactionsList = false;
                break;
            default:
                this.staffList = false;
                this.transactionsList = true;
                break;
        }
    }

    /**
     * Displays an action sheet to let the user choose whether to view or to void the selected transaction
     * Triggers when selecting a transaction from the transactions panel
     *
     */
    sortSalesPerHour(): void {
        this.hourlySales = [];
        var hourlySalesSummary = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        this.transactions.forEach(
            (transaction) => {

                var index = parseInt(transaction.created_at.substr(11, 2));
              
                hourlySalesSummary[index] = parseInt(transaction.grand_total) + hourlySalesSummary[index];
            }
        );

        var max = Math.max.apply(null, hourlySalesSummary) + 500;
        var hours = ['12am', '1am', '2am', '3am','4am', '5am', '6am','7am', '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm'];
        
        for (var i = 8; i < 23; i++) {
            this.hourlySales.push({'percentage': (hourlySalesSummary[i] /  max ) * 100, 'time': hours[i]});
        }

    }

    onSelectTransaction(transaction: Transaction): void {

        let actionsheet = this.actionctrl.create({
            buttons:[
                {
                    text: 'Preview Transaction',
                    role: 'destruction',
                    icon: 'eye',
                    handler: () => {
                        this.viewSingleTransaction(transaction);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: 'md-close',
                    handler: () => {
                        console.log('Cancelled');
                    }
                }
            ]
        });

        if (transaction.status == 'active') {

            actionsheet = this.actionctrl.create({
                buttons: [
                    {
                        text: 'Void Transaction',
                        role: 'destruction',
                        icon: 'ios-trash',
                        handler: () => {
                            this.showConfirm(transaction);

                        }
                    },
                    {
                        text: 'Preview Transaction',
                        role: 'destruction',
                        icon: 'eye',
                        handler: () => {
                            this.viewSingleTransaction(transaction);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        icon: 'md-close',
                        handler: () => {
                            console.log('Cancelled');
                        }
                    }

                ]
            });
        }

        actionsheet.present();
    }

    /**
     * Show a list of transactions of the selected staff
     * Triggers when selecting a staff from the branch summary panel on the 'Staff' tab
     *
     * @param {string} staffId
     * @returns {void}
     * @memberof FranchiseePage
     */
    showStaffTransactions(staffId: string): void {

        if (this.currentStaffId == staffId) {
            this.currentStaffId = null;
            return;
        }

        this.loadingStaffTransactions = true;
        this.currentStaffId = staffId;
        this.staffTransactions = [];

        const currentDateFilter = this.currentDateFilter;
        const branchKey = this.currentBranchKey;

        const options: SalesOptions = {
            branchKey: branchKey,
            rangeDate: currentDateFilter,
            staffId: staffId,
            limit: 'none'
        };

        this.transactionService.getTransactionHistory(options)
            .toPromise()
            .then(
                (response) => {
                    this.staffTransactions = response.data;
                    this.loadingStaffTransactions = false;
                }
            )
            .catch(
                (error) => {
                    console.log('Something went wrong while getting staff transactions', error);
                    this.loadingStaffTransactions = false;
                }
            );

    }

    /**
     * Displays the inventory of a branch
     * Triggers when selecting 'Inventory' from the navigation menu
     *
     * @memberof FranchiseePage
     */
    selectInventoryPanel(): void {
        this.panel = 'inventory';
        this.changePanelDateIndicator();
        this.currentBranchName = this.branches[0].name;
        this.currentBranchId = this.branches[0].id;
        this.loadInventory();
    }

    /**
     * Displays the inventory of the selected branch
     * Triggers when changing branches from the Inventory panel
     *
     * @param {number} index
     * @memberof FranchiseePage
     */
    onSelectBranchInventory(index: number): void {

        if (index >= 0) {
            this.currentBranchIndex = index;
            this.currentBranchName = this.branches[index].name;
            this.currentBranchId = this.branches[index].id;
        }

        this.warehouseProducts = [];
        this.loadInventory();
    }

    /**
     * Opens the default phone dialer to call Webforest support
     * Triggers when selecting 'Support' from the navigation menu
     *
     * @memberof FranchiseePage
     */
    selectCallSupport(): void  {
        this.callNumber.callNumber('09952028636', true)
          .then(response => console.log('Launched dialer!', response))
          .catch(error => console.log('Error launching dialer', error));
    }

    /**
     * Triggers when selecting 'Logout' from the navigation menu
     *
     * @memberof FranchiseePage
     */
    logout(): void {
        this.authService.logout();
        this.navCtrl.setRoot(SigninPage);
    }

    private async initializePage(): Promise<any> {
        this.storageService.remove(Constants.branchKeys);
        await this.checkForBranchKeys();
        await this.getFranchisee();
        this.getSalesChanges(this.currentRange);
    }

    private initializeSubscriptions(): void {
        this.subscribeToRoles();
        this.subscribeToRefreshPage();
    }

    private restartRefreshPageSubscription(): void {
        this.refreshPageSubscription.unsubscribe();
        this.subscribeToRefreshPage();
    }

    private terminateSubscriptions(): void {
        this.refreshPageSubscription.unsubscribe();
    }

    private subscribeToRoles(): void {
        this.updateRoleSubscription = this.trackingEventService.onUpdateRole
            .subscribe(
                (role: string) => {
                    this.role = role;
                },
                (error) => console.log('Error on role subscription', error)
            );
    }

    private subscribeToRefreshPage(): void {
        this.refreshPageSubscription = Observable.interval(60000)
            .subscribe(
                () => {
                    console.log('1 minute has passed, refreshing page...');
                    const panel = this.panel;

                    switch (panel) {
                        case 'transaction':
                            this.getSummaryByDatePerBranch(this.currentBranchKey);
                            break;
                        case 'inventory':
                            this.loadInventory();
                            break;
                        default:
                            this.initializePage();
                    }
                },
                (error) => console.log('Error on page refresh', error)
            );
    }

    private setCurrentDateFilter(): void {
        this.currentDateFilter = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
        this.changePanelDateIndicator();
    }

    private changePanelDateIndicator(): void {
        if (this.panel === 'home') {
            switch (this.currentRange) {
                case 'year':
                    this.dateSet = this.datePipe.transform(this.currentDate, 'y');
                    break;
                case 'month':
                    this.dateSet = this.datePipe.transform(this.currentDate, 'MMMM y');
                    break;
                default:
                    this.dateSet = this.datePipe.transform(this.currentDate, 'fullDate');
            }
        } else {
            this.dateSet = this.datePipe.transform(this.currentDate, 'fullDate');
        }
    }

    private getSummaryByDatePerBranch(branchKey: string | number): void {
        const loader = this.trackingEventService.loading('Loading transactions...');

        this.noTransactions = false;

        this.getTransactions(branchKey)
            .then(() => loader.dismiss())
            .catch(() => loader.dismiss());

        this.getBranchSummary(branchKey)
            .catch(() => loader.dismiss());

        this.getStaffDailySales(branchKey)
            .catch(() => loader.dismiss());
    }

    private setBranches(data: any[]): void {
        this.branches = [];
        this.totalFranchiseeSales.net  = 0;
        this.totalFranchiseeSales.gross  = 0;

        data.forEach(
            (jsonBranch) => {
                const branch = new Branch();
                branch.key = jsonBranch.key;
                branch.name = jsonBranch.name;
                branch.address = jsonBranch.address;
                branch.id = jsonBranch.id;
                branch.type = jsonBranch.type;
                branch.status = jsonBranch.status;
                branch.net_sales = jsonBranch.net_sales;
                this.totalFranchiseeSales.net = this.totalFranchiseeSales.net  + parseFloat(jsonBranch.net_sales);
                this.totalFranchiseeSales.gross = this.totalFranchiseeSales.gross  + parseFloat(jsonBranch.sales);
                this.totalFranchiseeSales.discounts = this.totalFranchiseeSales.gross -  this.totalFranchiseeSales.net;
                this.branches.push(branch);
            }
        );

        this.branches.sort(
            (a,b) => {

                if (parseFloat('' + a.net_sales) < parseFloat('' + b.net_sales)) {
                    return 1;
                }

                return 0;
            }
        );

        this.trackingEventService.onBranchKeyChange.emit(true);
    }

    private setStaffDailySalesSummary(jsonData: any[]): void {

        this.staffDailySalesSummary = jsonData.map(
            (staffJson: any) => {
                const staff = new Staff();
                staff.setStaff(staffJson);
                return staff;
            }
        );
    }

    private setWarehouseProducts(jsonData: any[]): void {
        this.warehouseProducts = [];
        this.productsContainer = [];

        jsonData.forEach((jsonProduct) => {

            const product = new ProductVariation();
            product.name = jsonProduct.name;
            product.size = jsonProduct.size;
            product.metrics = jsonProduct.metrics;
            product.product_id = jsonProduct.id;
            product.selling_price = parseInt(jsonProduct.selling_price);
            product.sold = 0;
            product.endInv = 0;
            product.begInv = 0;
            this.productsContainer.push(jsonProduct.id);

            if (this.currentBranchId) {
                product.quantity = jsonProduct.branch_quantity;
            }

            this.warehouseProducts.push(product);
        });
    }

    private setOptions(branchKey: string | number, type?: string): SalesOptions {
        const date = this.currentDateFilter;
        this.currentRange = 'day';
        this.setCurrentDateFilter();

        let options: SalesOptions = { subType: Constants.franchiseeFlag };

        if (typeof type !== 'undefined' && type === 'transactions') {
            options.branchKey = branchKey;
            options.limit = 0;
            options.rangeDate = date;
            return options;
        }

        options.keys = branchKey.toString();

        if (date) {
            options.from = date;
            options.to = date;
            options.subType = Constants.franchiseeFlag;
        }

        return options;
    }

    private viewSingleTransaction(transactionDetails): void {
        transactionDetails.branch_key = this.currentBranchKey;
        this.navCtrl.push(SingleTransactionPage, {transactionDetails: transactionDetails});
    }

    private showConfirm(transaction): void {
        const orNum = transaction.or_no;
        const title = 'VOID OR #' + orNum;
        const message = Constants.confirmVoidTransaction;

        let confirm = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancelled void');
                    }
                },
                {
                    text: 'Confirm',
                    handler: () => {
                        this.voidTransaction(transaction.id);
                    }
                }
            ]
        });

        confirm.present();
    }

    private voidTransaction(transactionId): void {
        const loader = this.trackingEventService.loading();

        this.transactionService.voidTransaction(transactionId)
            .subscribe(
                (response) => {
                    this.showAlert('void_success');
                    loader.dismiss();
                },
                (error) => {
                    console.log('Error voiding transaction', error);
                    this.showAlert(Constants.errorCode);
                    loader.dismiss();
                }
            );
    }

    // TODO: refactor and understand this
    private setUniqueBranchKeys(branchKeys: any) {
        if (!branchKeys) {
            return [];
        }

        if (branchKeys.length <= 0) {
            return branchKeys;
        }

        return branchKeys.filter(this.onlyUnique);
    }

    private onlyUnique(value, index, self): any {
        return self.indexOf(value) === index;
    }

    private showAlert(code: string, message?: string): void {
        let title: any;

        switch (code) {
            case 'no_internet':
                title = 'No Internet Connection';
                message = 'Please make sure you are connected to a network';
                break;

            case 'error':
                title = 'Error';
                message = 'Something unexpectedly went wrong. Please try again.'
                break;

            case 'void_success':
                title = 'Success';
                message = 'Transaction voided';
                break;

            default:
                title = 'Success';
                message = '';
        }

        let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'Okay',
                    handler: () => {
                        if (code === 'void_success') {
                            this.getSummaryByDatePerBranch(this.currentBranchKey);
                        }
                    }
                }
            ]
        });

        alert.present().catch((error) => console.log('Error on show alert', error));
    }

    private async checkForBranchKeys(): Promise<any> {
        this.keysExist = false;

        try {
            const branchKeys = await this.storageService.get(Constants.branchKeys);

            if (branchKeys && branchKeys.length > 0) {
                this.keysExist = true;
            }

        } catch (e) {
            console.log('Error checking branch keys', e);
            this.keysExist = false;
        }

    }

    private async getBranches(): Promise<any> {
        const loader = this.trackingEventService.loading('Loading Branches...');
        this.branches = [];

        try {
            const jsonBranchKeys = await this.storageService.get(Constants.branchKeys);
            const branchKeys = JSON.parse(jsonBranchKeys);
            this.branchKeys = branchKeys.join(',');

            if (!branchKeys) {
                this.keysExist = false;
                return false;
            }

            loader.dismiss();

            await this.getBranchesByKeyString(this.branchKeys);

        } catch (e) {
            loader.dismiss();
            console.log('Error retrieving branch keys from storage service', e);
        }

    }

    private async getBranchesByKeyString(keys: string): Promise<any> {
        this.setCurrentDateFilter();
        const dateFilter = this.currentDateFilter;

        const options: SalesOptions = {
            range: this.currentRange,
            rangeDate: dateFilter,
            subType: Constants.franchiseeFlag,
            keys: this.branchKeys
        };

        try {
            const summary = await this.transactionService.getBranchesSalesSummaryByOptions(options).toPromise();
            this.setBranches(summary.data);
            this.trackingEventService.dismissLoading();

        } catch (e) {
            console.log('Error retrieving branches', e);
            this.trackingEventService.dismissLoading();
        }

    }

    private async getBranchSummary(branchKey: string | number): Promise<any> {
        console.time('Branch Summary Loading Time');
        const options = this.setOptions(branchKey);

        try {
            const summary = await this.transactionService.getBranchesSalesSummaryByOptions(options).toPromise();
            this.dailySalesSummary = summary.data[0];
            this.loadingStaff = true;
            console.timeEnd('Branch Summary Loading Time');

        } catch (e) {
            console.log('Error retrieving branch sales summary', e)
        }
    }

    private async getBranchSalesSummary(options: SalesOptions): Promise<any> {
        const loader = this.trackingEventService.loading('Getting sales summary...');

        try {
            await this.getSalesChanges(options.range, options.rangeDate);
            const summary = await this.transactionService.getBranchesSalesSummaryByOptions(options).toPromise();
            loader.dismiss();
            this.setBranches(summary.data);

        } catch (e) {
            console.log('Error retrieving sales changes', e);
            loader.dismiss();
        }
    }

    private async getFranchisee(): Promise<any> {
        try {
            const email = await this.storageService.get('email');
            await this.getFranchiseeByEmail(email);

        } catch (e) {
            console.log('Error retrieving franchisee', e);
        }
    }

    private async getFranchiseeByEmail(email): Promise<any> {
        const loader = this.trackingEventService.loading('Getting User Info...');

        try {
            let franchiseeBranchKeys = [];
            const franchisee: { data: any[] } = await this.userService.getFranchiseeByEmail(email).toPromise();

            if (franchisee.data.length <= 0) {
                loader.dismiss();
                console.log('Franchisee account has no branches yet');

                const alert = this.alertCtrl.create({
                    title: 'Error',
                    message: 'Franchisee account has no branches yet',
                    buttons: [{ text: 'Okay', handler: () => this.logout() }],
                    enableBackdropDismiss: false
                });

                alert.present();

                return;
            }

            this.franchiseeName = franchisee.data[0].firstname + ' ' + franchisee.data[0].lastname;

            for (let branch of franchisee.data) {
                franchiseeBranchKeys.push(branch.key);
            }

            await this.storeDefaultFranchiseeBranchKey(franchiseeBranchKeys);
            loader.dismiss();

        } catch (e) {
            console.log('Error retrieving franchisee data', e);
            loader.dismiss();

            if (e.status == 0) {
                return this.showAlert('no_internet');
            }

            this.showAlert('error', 'Error in retrieving franchisee data');
        }
    }

    private async getSalesChanges(range: string = 'day', rangeDate: string = null): Promise<any> {
        const options: SalesOptions = {
            range: range,
            rangeDate: rangeDate,
            subType: Constants.franchiseeFlag,
            keys: this.branchKeys
        };

        try {
            const changes = await this.transactionService.getSalesChanges(options).toPromise();
            const state = changes.data.net_sales_state;
            this.currentSalesState = state;

            switch (state) {
                case 'increase':
                    this.totalNetSalesPercent = changes.data.net_sales_increase * 100;
                    break;
                default:
                    this.totalNetSalesPercent = changes.data.net_sales_decrease * 100;
            }

            this.restartRefreshPageSubscription();
            return changes;

        } catch (e) {
            console.log('Error retrieving sales changes', e);
        }

    }

    private async getStaffDailySales(branchKey: string | number): Promise<any> {
        console.time('Staff Daily Sales Loading Time');
        const options = this.setOptions(branchKey);

        try {
            const staffSales = await this.transactionService.getStaffDailySalesSummary(branchKey.toString(), options).toPromise();
            this.setStaffDailySalesSummary(staffSales.data);
            this.loadingStaff = false;
            console.timeEnd('Staff Daily Sales Loading Time');

        } catch (e) {
            console.log('Error retrieving staff sales', e);
        }
    }

    private async getTransactions(branchKey: string | number): Promise<any> {
        console.time('Transactions Loading Time');
        const options = this.setOptions(branchKey);
        const date = options.to;

        try {
            this.transactions = [];
            const transactions = await this.transactionService.getTransactionList(branchKey, date).toPromise();

            if (transactions.data.length <= 0) {
                this.noTransactions = true;
            }

            this.transactions = transactions.data;
             this.sortSalesPerHour();
            this.restartRefreshPageSubscription();
            console.log(this.transactions);
            console.timeEnd('Transactions Loading Time');

        } catch (e) {
            console.log('Error retrieving transactions', e);
        }
    }

    private async loadInventory(): Promise<any> {
        console.time('Inventory Loading Time');
        const loader = this.trackingEventService.loading('Loading inventory...');
        this.noInventory = false;
        this.warehouseProducts = [];

        try {
            const stocks = await this.productService.getProductStocks(null, this.currentBranchId).toPromise();
            this.setWarehouseProducts(stocks.data);
            const variationIds: string = this.productsContainer.join(',');
            const dailyInventory = await this.productService.getDailyInventory(this.currentDateFilter, this.currentBranchId, variationIds).toPromise();
            this.totalSoldItems = dailyInventory.data.total_sold_items;
            this.stockInventory = dailyInventory.data.inventory;
            loader.dismiss();
            this.restartRefreshPageSubscription();
            console.timeEnd('Inventory Loading Time');

        } catch (e) {
            console.log('Error loading inventory', e);
            this.noInventory = true;
            loader.dismiss();
        }
    }

    private async storeDefaultFranchiseeBranchKey(franchiseeBranchKeys: any[]): Promise<any> {
        let uniqueBranchKeys: any[] = [];

        try {
            const jsonBranchKeys = await this.storageService.get(Constants.branchKeys);
            let branchKeys = JSON.parse(jsonBranchKeys);
            uniqueBranchKeys = this.setUniqueBranchKeys(branchKeys);
            const index = uniqueBranchKeys.indexOf(franchiseeBranchKeys);

            if (index >= 0) {
                uniqueBranchKeys.splice(index, 1);
            }

            uniqueBranchKeys.unshift(franchiseeBranchKeys);
            await this.storageService.set(Constants.branchKeys, JSON.stringify(uniqueBranchKeys));
            await this.getBranches();

        } catch (e) {
            console.log('Error storing default franchisee branch key', e);
        }
    }

}
