import {DatePipe} from '@angular/common';
import {NavController} from 'ionic-angular';
import {Constants} from '../../../shared/constants';
import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

// CLASSES
import {Staff} from '../../../classes/staff.class';
import {Branch} from '../../../classes/branch.class';
import {Transaction} from '../../../classes/transaction.class';
import {SalesOptions} from '../../../classes/sales-options.class';

// SERVICES
import { BranchService, TrackingEventService, TransactionService } from '../../../shared/services';

@Component({
    selector: 'page-daily-transaction-details',
    templateUrl: 'daily-transaction-details.html'
})

export class DailyTransactionDetailsPage implements OnChanges {

    @Input() height: number = null;
    @Input() hidden: boolean = false;
    @Input() branchKey: number = null;

    lastFiveDays: any[] = [];
    currentDay: number = null;
    currentMonth: string = '';
    currentYear: number = null;
    branch: Branch = new Branch();
    currentStaffId: string = null;
    loadingStaff: boolean = false;
    currentDate: Date = new Date();
    currentDateFilter: string = null;
    staffDailySalesSummary: Staff[] = [];
    staffTransactions: Transaction[] = [];
    dailySalesSummary: Branch = new Branch();
    loadingStaffTransactions: boolean = false;

    isFranchisee: boolean = false;
    noStaffDailySummary: boolean = false;

    constructor(
        public navCtrl: NavController,
        private branchService: BranchService,
        private datePipe: DatePipe,
        private trackingEventService: TrackingEventService,
        private transactionService: TransactionService
    ) {}

    ngOnChanges(changes: SimpleChanges): void {
        this.checkIsBranchFranchisee()
            .then(
                () => {
                    const isVisible = !changes.hidden.currentValue;

                    if (isVisible) {
                        this.setLastFiveDays();
                        this.getSummaryByDate();
                    }
                }
            )
            .catch(error => console.log('Error', error));
    }


    // Template Functions

    getSummaryByDate(date: Date = new Date()): void {
        const loader = this.trackingEventService.loading('Loading transactions...');
        this.setCurrentDateVariables(date);
        this.currentStaffId = null;
        const branchKey = this.branchKey;
        const dateObj = this.getCurrentDateVariables(date);
        const branchKeyString = branchKey.toString();

        let options: SalesOptions = {
            keys: branchKeyString
        };

        if (dateObj.filterDate) {
            options = {
                keys: branchKeyString,
                from: dateObj.filterDate,
                to: dateObj.filterDate
            }
        }

        if (this.isFranchisee) {
            options.subType = Constants.franchiseeFlag;
        }

        this.transactionService.getBranchesSalesSummaryByOptions(options)
            .toPromise()
            .then(
                (response: any) => {
                    this.dailySalesSummary = response.data[0];
                    return this.transactionService.getStaffDailySalesSummary(branchKeyString, options).toPromise();
                }
            )
            .then(
                (response: any) => {
                    this.setStaffDailySalesSummary(response.data);

                    if (response.data.length <= 0) {
                        this.noStaffDailySummary = true;
                    }

                    loader.dismiss();
                }
            )
            .catch(
                (error) => {
                    loader.dismiss();
                    console.log('Error retrieving daily sales summary', error)
                }
            );
    }

    getStaffTransactionsById(staffId: string): void {

        if (this.currentStaffId == staffId) {
            this.currentStaffId = null;
            return;
        }

        this.currentStaffId = staffId;
        this.staffTransactions = [];
        this.loadingStaffTransactions = true;

        const currentDateFilter = this.currentDateFilter;
        const branchKey = this.branchKey;

        const options: SalesOptions = {
            branchKey: branchKey,
            rangeDate: currentDateFilter,
            staffId: staffId,
            limit: 'none'
        };

        this.transactionService.getTransactionHistory(options)
            .toPromise()
            .then(
                (response) => {
                    this.staffTransactions = response.data;
                    this.loadingStaffTransactions = false;
                }
            )
            .catch(
                (error) => {
                    console.log('Error retrieving staff transactions', error);
                    this.loadingStaffTransactions = false;
                }
            );
    }

    private checkIsBranchFranchisee(): Promise<any> {
        const key = this.branchKey;

        return this.branchService.getBranchByKey(key).toPromise()
            .then(
                (response) => {

                    if (response.data.type === 'franchisee') {
                        return this.isFranchisee = true;
                    }

                    return this.isFranchisee = false;
                }
            )
            .catch(
                (error) => {
                    console.log('Error checking if branch is franchisee', error);
                }
            );
    }

    private setCurrentDateVariables(date: Date = new Date()): void {
        const currentDateVariables = this.getCurrentDateVariables(date);
        this.currentDate = currentDateVariables.date;
        this.currentDay = currentDateVariables.day;
        this.currentMonth = currentDateVariables.month;
        this.currentYear = currentDateVariables.year;
        this.currentDateFilter = currentDateVariables.filterDate;
    }

    private getCurrentDateVariables(date: Date = new Date()): any {
        const day = date.getDate();
        const dayDisplay = this.datePipe.transform(date, 'dd');
        const month = this.datePipe.transform(date, 'MMMM');
        const dayString = this.datePipe.transform(date, 'EEEE');
        const year = date.getFullYear();
        const filterDate = this.datePipe.transform(date, 'yyyy-MM-dd');

        return {
            date: date,
            day: day,
            dayDisplay: dayDisplay,
            dayString: dayString,
            month: month,
            year: year,
            filterDate: filterDate
        };

    }

    private setStaffDailySalesSummary(jsonData: any[]): void {
        this.staffDailySalesSummary = jsonData.map(
            (staffJson: any) => {
                const staff = new Staff();
                staff.setStaff(staffJson);
                return staff;
            }
        );
    }

    private setLastFiveDays(): void {
        this.lastFiveDays = this.getPreviousDays();
    }

    private getPreviousDays(limit: number = 4): any[] {
        let previousDays: any[] = [];

        for (let noOfDays = limit; noOfDays >= 0; noOfDays--) {

            const currentDate = new Date();

            if (noOfDays == 0) {
                previousDays.push(this.getCurrentDateVariables(currentDate));
                continue;
            }

            currentDate.setDate(currentDate.getDate() - noOfDays);
            previousDays.push(this.getCurrentDateVariables(currentDate));

        }

        return previousDays;
    }


}
