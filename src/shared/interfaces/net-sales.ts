export interface NetSales {
    net_sales_state: string;
    net_sales_increase: number;
    net_sales_decrease: number;
}
