export interface SalesSummary {
    id: number;
    date: string;
    franchisee_sales: string;
    name: string;
    total_discounts: string;
    total_sales: string;
    total_sold_items: string;
}