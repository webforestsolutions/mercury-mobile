export interface SalesChange {    
    date: string;
    difference: number;
    percent: string;
    range: string;
    total_sales: number;
    type: string;
}
