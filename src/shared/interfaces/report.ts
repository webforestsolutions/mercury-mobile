export interface Report {
    day: string;
    month: string;
    sales_change: { difference: number, percent: string, percentage: string, total_sales: number, type: string };
    total_discounts: string;
    total_sales: string;
    total_sold_items: number;
}
