import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpService } from '../helpers/http.service';

@Injectable()
export class BranchService {

    basePath: string = 'api/branches';

    constructor(private httpService: HttpService) {}

    getBranches(filters?: any): Observable<any> {
        
        let endpoint: string = `${this.basePath}/get?essentials_only=true&exclude_dummy=true`;

        if (typeof filters !== 'undefined') {
            if (filters.source.trim().length > 0) {
                endpoint += `&source=${filters.source}`;
            }
        }

        return this.httpService.get(endpoint)
            .map((response: Response) => response.json());
    }

    getBranchesByKeys(branchKeysString: string = ''): Observable<any> {
        const requestUrl = this.basePath + '?keys=' + branchKeysString;

        return this.httpService.get(requestUrl)
            .map((response: Response) => response.json());
    }

    getBranchByKey(branchKey: any): Observable<any> {
        const requestUrl = this.basePath + '/key/' + branchKey;

        return this.httpService.get(requestUrl)
            .map((response: Response) => response.json());
    }
}
