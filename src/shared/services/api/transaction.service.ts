import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import 'rxjs/Rx';

import {Observable} from 'rxjs/Observable';

import {HttpService} from '../helpers/http.service';
import {Transaction} from '../../../classes/transaction.class';
import {Constants} from '../../constants';
import {SingleAdjustment} from '../../../classes/single-adjustment.class';
import {SalesOptions} from '../../../classes/sales-options.class';

@Injectable()
export class TransactionService {
    trackingPath: string = 'api/tracking/transactions';
    productsBasePath: string = 'api/products/transactions';
    adjustmentPath: string = 'api/tracking/transactions/products';
    transactionsPath: string = 'api/products/transactions';

    constructor(private httpService: HttpService) {}

    getTransactionHistory(options: SalesOptions): Observable<any> {

        const { branchKey, subType, from, to, rangeDate, staffId } = options;
        let { pageNumber, limit, query } = options;

        const excludeVoid = 1;
        const tracking = 1;
        const group = Constants.getSaleGroup;
        const sortBy = 'date';
        const order = 'desc';
        const range = 'daily';
        const excludeDelivery = 1;

        if (typeof pageNumber == 'undefined') {
            pageNumber = 1;
        }

        if (typeof limit === 'undefined') {
            limit = 10;
        }

        if (limit == 0) {
            limit = 'none';
        }

        let requestUrl = this.trackingPath + '?page=' + pageNumber + '&limit=' + limit;

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += '&sub_type=' + subType;
        }

        if (typeof query !== 'undefined' && query !== null && query !== ''){
            requestUrl += '&q=' + query;
        }

        if (typeof group !== 'undefined' && group !== null && group !== ''){
            requestUrl += '&group=' + group;
        }

        if (typeof sortBy !== 'undefined' && sortBy !== null){
            requestUrl += '&sort=' + sortBy;
        }

        if (typeof order !== 'undefined' && order !== null){
            requestUrl += '&order=' + order;
        }

        if (typeof range !== 'undefined' && range !== null){
            requestUrl += '&range=' + range;
        }

        if (typeof staffId !== 'undefined' && staffId !== null){
            requestUrl += '&staff_id=' + staffId;
        }

        if (typeof branchKey !== 'undefined' && branchKey !== null) {
            requestUrl += '&key=' + branchKey;
        }

        if (typeof excludeVoid !== 'undefined' && excludeVoid !== null && !isNaN(excludeVoid)){
            requestUrl += '&exclude_void=' + excludeVoid;
        }

        if (typeof tracking !== 'undefined' && tracking !== null && !isNaN(tracking)){
            requestUrl += '&tracking=' + tracking;
        }

        if (typeof excludeDelivery !== 'undefined' && excludeDelivery !== null && !isNaN(excludeDelivery)){
            requestUrl += '&exclude_delivery=' + excludeDelivery;
        }

        if (typeof from !== 'undefined' && from !== null && from !== '') {
            requestUrl += '&from=' + from;
        }

        if (typeof to !== 'undefined' && to !== null && to !== '') {
            requestUrl += '&to=' + to;
        }

        if (from == null && to == null && typeof range !== 'undefined' && range !== null) {
            requestUrl += '&range=' + range;
        }

        if (from == null && to == null && typeof rangeDate !== 'undefined' && rangeDate !== null) {
            requestUrl += '&range_date=' + rangeDate;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    getAllAdjustments(limit: number = 0, pageNumber?: number, query?: string): Observable<any> {

        const excludeVoid = 1;
        const group = Constants.adjustmentGroup;

        if (typeof pageNumber == 'undefined') {
            pageNumber = 1;
        }

        if (typeof limit == 'undefined') {
            limit = 10;
        }

        if (typeof query == 'undefined') {
            query = '';
        }

        let requestUrl = this.trackingPath + '?page=' + pageNumber + '&limit=' + limit + '&q=' + query + '&exclude_void=' + excludeVoid + '&group=' + group + '&tracking=1&range=monthly';

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    getTransactionList(branchKey: string | number, date: string): Observable<any> {
        let requestUrl: string = `api/transactions/branch/${branchKey}?date=${date}`;

        return this.httpService.get(requestUrl)
            .map((response: Response) => response.json());
    }

    findTransactionByOr(orNumber: string): Observable<any> {

        let requestUrl = this.trackingPath + '/receipts/' + orNumber;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    findTransactionById(transactionId: number): Observable<any> {

        let requestUrl = this.trackingPath + '/' + transactionId;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    addSaleTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.trackingPath + '/sale';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    returnSaleTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.trackingPath + '/sale/return';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    voidSaleTransaction(transactionId: number, staffId: number) {

        let body = {
            staff_id: staffId
        };

        const requestUrl = this.trackingPath + '/' + transactionId + '/void';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    voidReturnSaleTransaction(transactionId: number, staffId: number) {

        let body = {
            staff_id: staffId
        };

        const requestUrl = this.trackingPath + '/sale/return/' + transactionId + '/void';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    createSingleAdjustment(adjustment: SingleAdjustment) {

        let transaction = new Transaction();
        transaction.or_no = adjustment.or_no;
        transaction.staff_id = adjustment.staff_id;
        transaction.remarks = adjustment.getRemarksByCode(adjustment.remarks_code);

        adjustment.type = Constants.shortCode;

        transaction.setItem(adjustment.product_variation_id, adjustment.quantity);

        if (adjustment.remarks_code == Constants.shortOverCode) {
            transaction.setShortOverItem(adjustment.product_variation_id, adjustment.shortover_product_variation_id, adjustment.quantity);
            return this.addShortOverTransaction(transaction);
        }

        return this.addShortTransaction(transaction);
    }

    addShortTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.adjustmentPath + '/short';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );

    }

    addShortOverTransaction(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.adjustmentPath + '/shortover';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );

    }

    calculateDiscount(transaction: Transaction) {

        const body = JSON.stringify(transaction);

        const requestUrl = this.trackingPath + '/discounts/calculate';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json()
            );
    }

    getBranchesSalesSummary(range: string = 'day', subType: string = null, keys: string = null): Observable<any> {

        if (typeof range == 'undefined' || range == null) {
            range = 'day';
        }

        let requestUrl = this.transactionsPath + '/sales?range=' + range;

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += '&sub_type=' + subType;
        }

        if (typeof keys !== 'undefined' && keys !== null && keys !== '') {
            requestUrl += '&keys=' + keys;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    getBranchesSalesSummaryByOptions(options: SalesOptions): Observable<any> {

        // GET - api/products/transactions/sales

        const { subType, keys, from, to, range, rangeDate, span } = options;

        let requestUrl = this.transactionsPath + '/sales?version=1';

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += '&sub_type=' + subType;
        }

        if (typeof keys !== 'undefined' && keys !== null && keys !== '') {
            requestUrl += '&keys=' + keys;
        }

        if (typeof from !== 'undefined' && from !== null && from !== '') {
            requestUrl += '&from=' + from;
        }

        if (typeof to !== 'undefined' && to !== null && to !== '') {
            requestUrl += '&to=' + to;
        }

        if (from == null && to == null && typeof range !== 'undefined' && range !== null) {
            requestUrl += '&range=' + range;
        }

        if (from == null && to == null && typeof rangeDate !== 'undefined' && rangeDate !== null) {
            requestUrl += '&range_date=' + rangeDate;
        }

        if (typeof span !== 'undefined') {
            requestUrl += `&span=${span}`;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    getStaffDailySalesSummary(keys: string = null, { range = null, subType = null, from = null, to = null }: { range?: string, subType?: string, from?: string, to?: string } = {}): Observable<any> {

        if (typeof range == 'undefined' || range == null) {
            range = 'day';
        }

        let requestUrl = this.transactionsPath + '/staff/sales?version=1';

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += '&sub_type=' + subType;
        }

        if (typeof keys !== 'undefined' && keys !== null && keys !== '') {
            requestUrl += '&keys=' + keys;
        }

        if (typeof from !== 'undefined' && from !== null && from !== '') {
            requestUrl += '&from=' + from;
        }

        if (typeof to !== 'undefined' && to !== null && to !== '') {
            requestUrl += '&to=' + to;
        }

        if (from == null && to == null && range !== 'undefined' && range !== null) {
            requestUrl += '&range=' + range;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    getSalesChanges(options: SalesOptions): Observable<any> {

       const { subType, keys, from, to, range, rangeDate } = options;

        let requestUrl = this.transactionsPath + '/sales/summary/changes?version=1';


        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += '&sub_type=' + subType;
        }

        if (typeof keys !== 'undefined' && keys !== null && keys !== '') {
            requestUrl += '&keys=' + keys;
        }

        if (typeof from !== 'undefined' && from !== null && from !== '') {
            requestUrl += '&from=' + from;
        }

        if (typeof to !== 'undefined' && to !== null && to !== '') {
            requestUrl += '&to=' + to;
        }

        if (from == null && to == null && typeof range !== 'undefined' && range !== null) {
            requestUrl += '&range=' + range;
        }

        if (from == null && to == null && typeof rangeDate !== 'undefined' && rangeDate !== null) {
            requestUrl += '&range_date=' + rangeDate;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    getTopSaleItems(options: SalesOptions): Observable<any> {

        const { subType, branchId, range, rangeDate } = options;
        let { limit } = options;

        if (!limit || typeof limit == 'undefined') {
            limit = 7;
        }

        let requestUrl = this.productsBasePath + '/sales/top?version=1';

        if (typeof range !== 'undefined' && range != null) {
            requestUrl += '&range=' + range;
        }

        if (typeof branchId !== 'undefined' && branchId !== null && !isNaN(branchId)) {
            requestUrl += '&branch_id=' + branchId;
        }

        if (typeof subType !== 'undefined' && subType !== null && subType !== '') {
            requestUrl += '&sub_type=' + subType;
        }

        if (typeof limit !== 'undefined' && limit !== null) {
            requestUrl += '&limit=' + limit;
        }

        if (typeof rangeDate !== 'undefined' && rangeDate !== null) {
            requestUrl += '&range_date=' + rangeDate;
        }

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    voidTransaction(transactionId: number) {

        let body = {};

        const requestUrl = this.productsBasePath + '/' + transactionId + '/void';

        return this.httpService.post(requestUrl, body)
            .map(
                (response: Response) => response.json().data
            );
    }

    getBranchSalesSummary(type: string, date: string, range: string): Observable<any> {
        return this.httpService.get(`api/sales/branch/${type}/summary/${date}/${range}`)
            .map((response: Response) => response.json());
    }

    getBranchSalesSummaryPerDateByPeriod(branchId: number, from: string, to: string): Observable<any> {
        const request: string = `api/sales/branch/${branchId}/period?from=${from}&to=${to}`;

        return this.httpService.get(request)
            .map((response: Response) => response.json());
    }

    getTotalSalesSummary(date: string, range: string): Observable<any> {
        return this.httpService.get(`api/sales/summary/change/${date}/${range}`)
            .map((response: Response) => response.json());
    }

    getTotalMonthlyBranchSalesForOneYear(date: string, range: string): Observable<any> {
        return this.httpService.get(`api/sales/report/${date}/${range}`)
            .map((response: Response) => response.json());
    }

}
