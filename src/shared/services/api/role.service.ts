import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";

import {HttpService} from '../helpers/http.service';

@Injectable()
export class RoleService {

    basePath: string = '/api/users';

    constructor(private httpService: HttpService) {

    }

    getStaffRoles(code: string) {
        const requestUrl = this.basePath + '?role=staff';
        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response: ', response.json());
                    return response.json();
                }
            )
    }

    getMemberRoles(code: string) {
        const requestUrl = this.basePath + '?role=member';
        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response ', response.json());
                    return response.json();
                }
            )
    }

    getCompanyStaffRoles(code: string) {
        const requestUrl = this.basePath + '?role=company_staff';
        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response ', response.json());
                    return response.json();
                }
            )
    }

    getCompanyStaffPermissions(id: number) {
        const requestUrl = this.basePath + '/permissions/' + id;
        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    console.log('response ', response.json());
                    return response.json();
                }
            )

    }

}