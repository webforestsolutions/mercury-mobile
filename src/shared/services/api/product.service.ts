import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {HttpService} from '../helpers/http.service';

@Injectable()
export class ProductService {

    basePath: string = 'api/products';

    constructor(private httpService: HttpService) {}

    getProducts(pageNumber: number, categoryId?: number, query?: string, limit?: number): Observable<any> {

        if (typeof pageNumber == 'undefined') {
            pageNumber = 1;
        }

        if (typeof limit == 'undefined') {
            limit = 10;
        }

        if (typeof query == 'undefined') {
            query = '';
        }

        let requestUrl = this.basePath + '?page=' + pageNumber + '&limit=' + limit + '&q=' + query;

        if (typeof categoryId !== 'undefined' && !isNaN(categoryId) && categoryId != null) {
            requestUrl += '&category_id=' + categoryId;
        }

        return this.httpService.get(requestUrl)
            .map((response: Response) => {
                return response.json();
            });
    }

    getProductStocks(query?: string, branchId:number = null, limit: string = 'none'): Observable<any> {

        let path = this.basePath + '/stocks';

        if (typeof branchId !== 'undefined' && branchId !== null && !isNaN(branchId)){
            path += '/branches/'+branchId;
        }

        let requestUrl =  path + '?' + 'limit=' + limit;

        if (typeof query !== 'undefined' && query !== null && query !== '') {
            requestUrl += '&q=' + query;
        }

        return this.httpService.get(requestUrl)
            .map((response: Response) => {
                return response.json();
            });
    }

    getDailyInventory(date, branchId: number = null, products: string, limit: string = 'none'): Observable<any> {

        // * api/products/inventory/daily

        let path = this.basePath + '/inventory/daily';

        let requestUrl =  path + '?' + 'limit=' + limit + '&source=mobile';

        if (typeof branchId !== 'undefined' && branchId !== null && !isNaN(branchId)){
            requestUrl += '&branch_id=' + branchId;
        }

        if (typeof date !== 'undefined' && date != null) {
            requestUrl += '&date=' + date;
        }

        if (typeof products !== 'undefined' && products != null) {
            requestUrl += '&product_variation_id=' + products;
        }

        return this.httpService.get(requestUrl)
            .map((response: Response) => {
                return response.json();
            });
    }

}
