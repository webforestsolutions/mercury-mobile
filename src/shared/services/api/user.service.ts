import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import "rxjs/Rx";

import {HttpService} from "../helpers/http.service";
import {User} from '../../../classes';
import {Observable} from "rxjs/Observable";

@Injectable()
export class UserService {

    basePath: string = 'api/tracking/users';
    authBasePath: string = 'api/users';

    constructor(private httpService: HttpService) {}

    getCustomers(pageNumber: number, query?: string, limit?: string): Observable<any> {

        if (typeof pageNumber == 'undefined') {
            pageNumber = 1;
        }

        if (typeof limit == 'undefined') {
            limit = 'none';
        }

        if (typeof query == 'undefined') {
            query = "";
        }

        let requestUrl = this.basePath + "?page=" + pageNumber + "&limit=" + limit + "&q=" + query;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    getTopDealers(period: string = 'month', date: string): Observable<any> {
        const requestUrl = this.authBasePath + `/dealers/top?period=${period}1&date=${date}`;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    getCustomerById(customerId:string): Observable<any> {

        const requestUrl = this.basePath+'/customers/'+customerId;

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    createGuest(user: User): Observable<any> {

        const userData = JSON.stringify(user);

        const requestUrl = this.basePath+'/guests';

        return this.httpService.post(requestUrl, userData)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    createCustomer(user: User, staffId:string): Observable<any> {

        user.staff_id = staffId;
        const userData = JSON.stringify(user);

        const requestUrl = this.basePath+'/customers';

        return this.httpService.post(requestUrl, userData)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }

    getFranchiseeByEmail(email: any): Observable<any> {

        const requestUrl = 'api/users/email';

        let emailObj: object = {
            email: email
        };

        const userData = JSON.stringify(emailObj);

        return this.httpService.post(requestUrl, userData)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );

    }

    getCurrentUser(): Observable<any> {

        const requestUrl = this.authBasePath+'/current';

        return this.httpService.get(requestUrl)
            .map(
                (response: Response) => {
                    return response.json();
                }
            );
    }
}
