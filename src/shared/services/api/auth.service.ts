import { EventEmitter, Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

import { Constants } from '../../constants';
import { StorageService } from '../helpers/storage.service';

@Injectable()
export class AuthService {
    basePath: string = 'api/auth/mobile';
    baseUrl = Constants.baseUrl + this.basePath;
    confirm = new EventEmitter<boolean>();
    refreshTokenEvent = new EventEmitter<boolean>();
    token: string = '';

    private adminRole: string = Constants.adminRole;
    private companyRole: string = Constants.companyRole;

    constructor(
        private http: Http,
        private storage: StorageService
    ) { }

    public authenticate(): Observable<boolean> {
        // do something to make sure authentication token works correctly
        // return Observable.of(true);
        // removed since we will not check for session anymore
        if (!this.isTokenExpired()) {
            return Observable.of(true);
        }

        this.refreshTokenEvent.emit(true);

        return Observable.create(
            observer => {
                this.confirm.subscribe(
                    request => {
                        observer.next(request);
                        observer.complete();
                    }
                );
            }
        );
    }

    async getTokenAsync(): Promise<any> {
        const response = await this.storage.get('token');
        return response;
    }

    signUp(email: string, password: string, firstname: string, lastname: string): Observable<any> {
        const userData = { email: email, password: password, firstname: firstname, lastname: lastname };
        const headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.post(this.baseUrl + '/register', userData, {headers: headers});
    }

    signIn(email: string, password: string): Observable<any> {
        const userData = { email: email, password: password };
        const headers = new Headers({ 'Content-Type': 'application/json' });

        return this.http.post(this.baseUrl + '/login', userData, {headers: headers})
            .map(
                (response: Response) => {
                    this.token = response.json().token;
                    const decoded: { role: string } = this.decodeToken(this.token);
                    this.storeEmail(userData.email);
                    return { token: this.token, decoded: decoded, role: decoded.role };
                }
            )
            // do is executed everytime the subscribe fires
            .do(
                tokenData => this.storage.set('token', tokenData.token).catch((error) => console.log('Error setting token', error))
            );
    }

    refreshTokenFromExpiredRequest(): void {
        this.getToken()
            .then(
                (token) => {

                    if (!this.isTokenExpired(token)) {
                        return null;
                    }

                    return this.refreshToken(token).toPromise();
                }
            )
            .then(() => this.confirm.emit(true))
            .catch((error) => console.log('Error refreshing token from expired request ', error));
    }

    refreshToken(token): Observable<any> {
        const data = {};
        const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token });

        return this.http.post(this.baseUrl + '/refresh', data, {headers: headers})
            .map(
                (response: Response) => {
                    this.token = response.json().token;
                    const decoded: { role: string } = this.decodeToken(token);
                    return { token: this.token, decoded: decoded, role: decoded.role };
                }
            )
            // do is executed everytime the subscribe fires
            .do( 
                tokenData => this.storage.set('token', tokenData.token).catch((error) => console.log('Error refreshing token', error))
            );
    }

    storeEmail(email: string): void {
        this.storage.set('email', email)
            .catch(
                error => console.log('Error storing email', error)
            );
    }

    isLoggedIn(): boolean {
        const token = this.token;

        if (typeof token === 'undefined' || token === null) {
            return false;
        }

        const decodedTokenData = this.decodeToken(token);

        if (typeof decodedTokenData.exp == 'undefined') {
            return false;
        }

        return true;
    }

    isTokenExpired(token: string = null): boolean {

        if (!token) {
            token = this.token;
        }

        if (typeof token === 'undefined' || token === null) {
            return true;
        }

        const decodedTokenData = this.decodeToken(token);

        if (typeof decodedTokenData.exp == 'undefined') {
            return true;
        }

        const expTimestamp = decodedTokenData.exp * 1000;
        const todaysTimestamp = new Date().getTime();

        if (expTimestamp < todaysTimestamp) {
            return true;
        }

        return false;
    }

    isValidRole(role: string): boolean {
        const token = this.token;

        if (typeof token === 'undefined' || token === null) {
            return false;
        }

        const decodedTokenData = this.decodeToken(token);

        if (typeof decodedTokenData.role == 'undefined') {
            return false;
        }

        if (decodedTokenData.role !== role) {
            return false;
        }

        return true;
    }

    removeToken(): void {
        this.storage.remove('token');
    }

    setToken(token: string): void {
        this.token = token;
    }

    getToken(): Promise<any> {
        return this.storage.get('token');
    }

    getPermissions(): string[] {
        const token = this.token;

        let permissions = [];

        if (typeof token === 'undefined' || token === null) {
            return permissions;
        }

        const decodedTokenData = this.decodeToken(token);

        if (typeof decodedTokenData.role == 'undefined') {
            return permissions;
        }

        return decodedTokenData.permissions;
    }

    getStaffPrivileges(): string[] {
        let privileges = [];
        const token = this.token;

        if (typeof token === 'undefined' || token === null) {
            return privileges;
        }

        const decodedTokenData = this.decodeToken(token);

        if (typeof decodedTokenData.role === 'undefined') {
            return privileges;
        }

        return decodedTokenData.staff_privileges;
    }

    decodeToken(token): any {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }

    validateUserPrivileges(roleCodes: string[]): boolean {
        const userPrivileges = roleCodes;
        const authService = this;
        let hasPrivilege = false;

        userPrivileges.forEach(
            (role) => {
                const isValidRole = authService.isValidRole(role);

                if (isValidRole) {
                    hasPrivilege = isValidRole;
                }
            }
        );

        return hasPrivilege;
    }

    isOwner(): boolean {
        return this.validateUserPrivileges([this.adminRole, this.companyRole]);
    }

    isFranchisee(): boolean {
        const code = Constants.franchiseeFlag;
        const privileges = this.getStaffPrivileges();
        let hasPrivileges = false;

        privileges.forEach(
            (permission) => {
                if (permission === code) {
                    hasPrivileges = true;
                }
            }
        );

        return hasPrivileges;
    }

    isCoordinator(): boolean {
        const code = Constants.coordinatorFlag;
        const privileges = this.getStaffPrivileges();
        let hasPrivileges = false;

        privileges.forEach(
            (permission) => {
                if (permission === code) {
                    hasPrivileges = true;
                }
            }
        );

        return hasPrivileges;
    }

    getRole(): string {
        const isOwner = this.isOwner();

        if (isOwner) {
            return Constants.ownerFlag;
        }

        const isFranchisee = this.isFranchisee();

        if (isFranchisee) {
            return Constants.franchiseeFlag;
        }

        const isCoordinator = this.isCoordinator()

        if (isCoordinator) {
            return Constants.coordinatorFlag;
        }

    }

    logout(): void {
        this.removeToken();
        this.storage.remove(Constants.branchKeys);
        this.storage.remove(Constants.currentRole);
        this.storage.remove('email');
    }
}
