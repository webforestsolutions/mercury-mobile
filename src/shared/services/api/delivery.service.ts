import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { HttpService } from '../helpers/http.service';

@Injectable()
export class DeliveryService {

    private basePath: string = 'api/deliveries';
    private productDeliveriesPath: string = 'api/products/deliveries';

    constructor(private httpService: HttpService) { }

    getDeliveries(): Observable<any> {
        const endpoint: string = `${this.basePath}?source=mobile`;

        return this.httpService.get(endpoint)
            .map((response: Response) => response.json());
    }

    confirmDelivery(id: number): Observable<any> {
        const endpoint: string = `${this.productDeliveriesPath}/${id}/confirm`;

        return this.httpService.post(endpoint, {})
            .map((response: Response) => response.json().data);
    }

    payDelivery(id: number): Observable<any> {
        const status: string = 'paid';
        const endpoint: string = `${this.basePath}/payment/status`;

        return this.httpService.put(endpoint, { id: id, status: status})
            .map((response: Response) => response.json());
    }

}