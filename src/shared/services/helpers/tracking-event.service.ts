import { EventEmitter, Injectable } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';

@Injectable()
export class TrackingEventService {
    loader: Loading = null;
    onBranchKeyChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    onBranchListChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    onCurrentBranchKeyChange: EventEmitter<string> = new EventEmitter<string>();
    onUpdateRole: EventEmitter<string> = new EventEmitter<string>();

    constructor(
        private appVersion: AppVersion,
        private loadingCtrl: LoadingController
    ) { }

    async getAppName(): Promise<any> {
        return await this.appVersion.getAppName();
    }

    async getAppVersion(): Promise<any> {
        return await this.appVersion.getVersionNumber();
    }

    loading(alertString: string = 'Please wait...'): Loading {
        this.loader = this.loadingCtrl.create({ content: alertString });
        this.loader.present();
        return this.loader;
    }

    dismissLoading(loader?: any): void {

        if (typeof loader !== 'undefined') {
            loader.dismiss();
        }

        this.loader = null;
    }

}
