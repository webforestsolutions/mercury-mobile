export class Page {

    id?:number;
    key?:string;
    title: string;
    component: any;
    role?: any;

    constructor() {
        this.title = "";
    }

    setId(id: number) {
        this.id = id;
    }

    setTitle(title: string){
        this.title = title;
    }

    setComponent(component: any) {
        this.component = component;
    }

    setKey(key: string) {
        this.key = key;
    }

    setRole(role: any) {
        this.role = role;
    }

}
