export class SalesChange {
    date: string;
    difference: number;
    percent: string;
    range: string;
    total_sales: number;
    type: string;

    constructor() {
        this.difference = 0;
        this.percent = '0.00%';
        this.range = 'daily';
        this.total_sales = 0.00;
        this.type = 'decrease';
    }
}
