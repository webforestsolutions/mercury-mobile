import {User} from "./user.class";

export class Staff extends User {

    constructor(){
        super();
    }

    can_void: number;
    has_multiple_access: number;

    net_sales: number;
    sales: number;
    returns: number;
    discounts: number;
    sold_items: number;

    setStaff(jsonData: any) {

        this.staff_id = jsonData.staff_id;
        this.firstname = jsonData.firstname;
        this.lastname = jsonData.lastname;
        this.can_void = jsonData.can_void;
        this.has_multiple_access = jsonData.has_multiple_access;

        this.net_sales = jsonData.net_sales;
        this.sales = jsonData.sales;
        this.returns = jsonData.returns;
        this.discounts = jsonData.discounts;
        this.sold_items = jsonData.sold_items;

    }

}