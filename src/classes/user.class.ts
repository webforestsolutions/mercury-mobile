export class User {

    constructor(){
        this.firstname = "";
        this.lastname = "";
        this.email = "";
        this.branch_id = null;
        this.branch_id_registered = null;
        this.password = "";
        this.phone = "";
        this.address = "";
        this.city = "";
        this.province = "";
        this.zip = "";
        this.permissions = [];

    }

    id?: number;
    role: string;
    staff_id: string;
    firstname: string
    lastname: string;
    branch_name: string;
    email: string;
    password: string;
    branch_id: number;
    branch_id_registered: number;
    phone: string;
    address: string;
    city: string;
    province: string;
    zip:string;
    permissions:any[];
    confirm_password:string;

    getInitials() {

        const firstInitial = this.firstname.charAt(0);
        const secondInitial = this.lastname.charAt(0);

        return firstInitial + secondInitial;

    }

}