import { ProductVariation } from "./product-variation.class";

export class Transaction {
    branch_key?: any;
    branch_id: number;
    created_at: string;
    customer_id: string;
    customer_user_id: number;
    customer_firstname: string;
    customer_lastname: string;
    customer_phone: string;
    discount: number = 0;
    grand_total?: string;
    has_returns: number = 0;
    id?: number;
    isViewed: boolean;
    items: ProductVariation[] = [];
    invoice_no: any;
    items_string: string;
    key: string;
    net_sales: number;
    or_no: string;
    price_rule_id: number;
    price_rule_code: string;
    returns: ProductVariation[] = [];
    remarks: string;
    staff_id: number;
    staff_firstname: string;
    staff_lastname: string;
    shortover_string: string;
    status: string;
    sale_type: string;
    sold_items: number;
    subtotal:number = 0;
    time?: any;
    transaction_type: string;
    transaction_type_name: string;
    transaction_type_id: number;
    total:number = 0;
    user_id: number;
    vat_price: number = 0;
    vat_percentage: number = 0;

    constructor() {
        this.isViewed = false;
        this.items = [];
    }

    setEmptyItems(): void {
        this.items = [];
    }

    isEmpty(): boolean {

        if (typeof this.items == null) {
            this.items = [];
        }

        if (this.items.length > 0) {
            return false;
        }

        return true;

    }

    addToCart(item: ProductVariation): boolean|void {

        if (item.isSelected) {
            return false;
        }

        item.isSelected = true;
        item.quantity = 1;
        this.items.push(item);
        this.computeTotal();
    }

    removeCartItem(index: number): void {
        this.items.splice(index, 1);
        this.computeTotal();
    }

    addQuantity(item: ProductVariation): boolean|void {

        const itemAdded = item.addQuantity();

        if (!itemAdded) {
            return false;
        }

        this.computeTotal();

        return true;

    }

    removeQuantity(item: ProductVariation, force: boolean = false): boolean {
        const itemRemoved = item.removeQuantity(force);

        if (!itemRemoved) {
            return false;
        }

        this.computeTotal();

        return true;
    }

    computeTotal(): number|string {
        this.total = 0;

        this.items.forEach((item) => {
            this.total += parseFloat('' + item.selling_price) * parseFloat('' + item.quantity);
        });

        if (isNaN(this.total)) {
            return 0;
        }

        this.total.toFixed(2);

        return this.total;
    }

    calculateSubtotalMinusVat(): number|void {
        const subTotalMinusVat = parseFloat('' + this.total) - parseFloat('' + this.vat_price);

        if (isNaN(subTotalMinusVat)) {
            return 0;
        }

        this.subtotal = subTotalMinusVat;
    }

    calculateVatPrice(vat: number): number|void {

        const vatPrice = parseFloat('' + this.total) * parseFloat('' + vat);

        if (isNaN(vatPrice)) {
            return 0;
        }

        this.vat_price = vatPrice;

    }

    calculateVatPercentage(vat: number): number|void {
        const vatPercentage = parseFloat(''+vat)*100;

        if (isNaN(vatPercentage)) {
            return 0;
        }

        this.vat_percentage = vatPercentage;
    }

    setItem(productVariationId: number, quantity: number): void {
        this.setEmptyItems();
        const productVariation = new ProductVariation();
        productVariation.product_variation_id = productVariationId;
        productVariation.quantity = quantity;
        this.items.push(productVariation);
    }

    setShortOverItem(productVariationId: number, shortOverProductVariationId: number, quantity: number): void {
        this.setEmptyItems();
        const productVariation = new ProductVariation();
        productVariation.product_variation_id = productVariationId;
        productVariation.shortover_product_variation_id = shortOverProductVariationId;
        productVariation.quantity = quantity;
        this.items.push(productVariation);
    }
}
