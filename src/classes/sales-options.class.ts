export class SalesOptions {
    keys?: string;
    range?: string;
    subType?: string;
    from?: string;
    to?: string;
    rangeDate?: string;
    branchKey?: number|string;
    branchId?:number;
    limit?: number | string;
    query?:string;
    pageNumber?:number;
    staffId?: string;
    span?: string;

    constructor() {
        this.keys = null;
        this.range = 'day'; // day, month, year
        this.subType = null;
        this.from = null;
        this.to = null;
        this.rangeDate = null;
    }
}
