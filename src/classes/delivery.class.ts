export class Delivery {

    id?: number;
    status?: string;
    created_at?: string;
    delivery_date: string;
    invoice_no?: string;
    branch_id?: number;
    branch_name?: string;
    quantity: number;
    remarks?: string;
    payment_status: string;

    constructor() {
        this.id = null;
        this.status = '';
        this.delivery_date = '';
        this.branch_id = null;
        this.branch_name = '';
        this.quantity = null;
        this.payment_status = '';
    }

}