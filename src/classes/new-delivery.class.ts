
import {DeliveryItem} from "./delivery-item.class";
export class NewDelivery {

    constructor(){
        this.branch_id = null;
        this.deliveries.push(new DeliveryItem());
    }

    id?: number;
    branch_id: number;
    deliveries: Array<DeliveryItem> = [];

    addNewDeliveryItem(){

        const deliveries = this.deliveries;
        const length = deliveries.length;
        const last = deliveries[length-1];

        if(last.isValid()){
            deliveries.push(new DeliveryItem());
        }

    }

    setNewDeliveries(deliveriesJson: any[]){

        this.deliveries = [];

        deliveriesJson.forEach((deliveryItem) => {
            const item = new DeliveryItem();
            item.product_variation_id = deliveryItem.product_variation_id;
            item.quantity = deliveryItem.quantity;
            this.deliveries.push(item);
        });

    }
}